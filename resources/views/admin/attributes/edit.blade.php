@extends('layouts.admin')

@section('content')

	<h1>Attribute</h1>

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

    <ul class="nav nav-tabs">
		<li role="presenstation" class="active"><a href="#"><strong>Attribute Lists - Edit</strong></a></li>
	</ul>

    <div class="container " style="padding: 10px;">
        
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Attribute Lists
                </div>

                <div class="panel-body">
                    <!-- Category List -->
                    <table class="table table-striped text-center">
                        <thead">
                            <th class="text-center">ID</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Actions</th>
                        </thead>

                        <tbody>
                            @foreach($attributes as $attributelist)
                            <tr>
                                <td>{{ $attributelist->id }}</td>
                                <td>{{ $attributelist->name }}</td>
                                <td>
                                <a href="{{ route('admin.attributes.edit', $attribute->id) }}" class="btn btn-primary btn-xs" disabled><i class="material-icons">edit</i></a>
                                <form action="{{ route('admin.attributes.destroy', $attribute->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                    <button class="btn btn-danger btn-xs" disabled><i class="material-icons">delete</i></button>
                                </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Attribute
                </div>

                <div class="panel-body">
                    <!-- Edit Attribute -->
                    <form action="{{ route('admin.attributes.update', $attribute->id) }}" method="POST" class="form-horizontal">
                    @csrf
                    @method('PATCH')

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="name" class="control-label">Attribute</label>
                                <input type="text" name="name" id="name" class="form-control" value="{{ $attribute->name }}">
                            </div>
                        </div>

                        <div class="row clearfix" style="float:right;">
                            <button type="submit" class="btn btn-success">
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
