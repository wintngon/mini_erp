<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    // protected $guarded = [];
    use SoftDeletes;

    public function products(){
        return $this->hasMany('App\Product');
    }
}
