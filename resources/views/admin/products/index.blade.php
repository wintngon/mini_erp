@extends('layouts.admin')

@section('content')

{{ Breadcrumbs::render('products', $products) }}
	<h1>Production</h1>

	@include('includes.message')

	@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

	<!-- Start .nav nav-tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presenstation" class="active"><a href="#view" aria-controls="view" role="tab" data-toggle="tab"><strong>View Products</strong></a></li>
		<li role="presenstation"><a href="#create" aria-controls="create" role="tab" data-toggle="tab"><strong>Create Product</strong></a></li>
	</ul>
	<!-- End .nav nav-tabs -->

	<div class="tab-content">
		<!-- Show Products -->
		<div role="tabpanel1" class="tab-pane fade in active" id="view">
			<div class="row" style="padding: 20px;">	
				<form action="{{ route('admin.products.search') }}">
					<div class="col-md-12">
						<div class="form-group col-md-4">
							<label>Keyword</label>
							<input type="text" name="name" id="keyword" class="name form-control" value="{{ old('name', Input::get('name') != null ? Input::get('name') : '' ) }}">
							<div id="name_list" style="position:relative;"></div>
						</div>

						<div class="form-group col-md-3">
							<label>Category</label>
							<select name="category" id="category" class="form-control"> 
							<option value="">Select category</option>
							@foreach($categories as $category)
								<option value="{{ $category->id }}" {{ Input::get('category') == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
							@endforeach
							</select>
						</div>

						<div class="form-group col-md-3">
							<label>Attribute</label>
							<select name="attribute" id="attribute" class="form-control"> 
							<option value="">Select Attribute</option>
							@foreach($attributes as $attribute)
								<option value="{{ $attribute->id }}" {{ Input::get('attribute') == $attribute->id ? 'selected' : '' }}>{{ $attribute->name }}</option>
							@endforeach
							</select>
						</div>

						<div class="form-group col-md-2">
							<label>Stock</label>
							<input type="checkbox" name="qty" id="qty" class="form-control" {{ Input::get('qty') == 'on' ? 'checked' : '' }}>
						</div>
					</div>

					<button type="submit" name="clear" value="clear" class="clear_search btn btn-default" style="float:right; margin-top:20px;">Clear</button>
					<button type="submit" name ="export"  value="export" class="btn btn-info" style="float:right; margin-top:20px;">Export</button>
					<input type="submit" value="Search" class="btn btn-success" style="float:right; margin-top:20px;">
					
				</form>	
			</div>

			<hr>
			<div class="row">
				<div class="table-responsive col-md-12">
				<table class="table table-bordered">
					<thead>
					<tr>
						<th>Id</th>
						<th>Image</th>
						<th>Name</th>
						<th>Description</th>
						<th>Category</th>
						<th>Attribute</th>
						<th>Price</th>
						<th>Qty</th>
						<th>Stock-status</th>
						<th>Actions</th>
					</tr>
					</thead>
					<tbody>	
					@foreach($products as $product)
						<tr>
							<td>{{ $product->id }}</td>
							<td >
							@foreach($product->media as $image)
							@if($loop->first)
								<img src="{{ url('uploads/'.$image->file_name.'.'.$image->file_extension) }}" alt="image-alt" style="max-height: 75px; border: 2px solid; padding: 1px; width: 100px; height: 100px;background-size: cover; background-repeat:no-repeat; display: inline-block; ">
							@endif
							@endforeach
							</td>
							<td>{{ $product->name }}</td>
							<td>{{ $product->description }}</td>
							<td>
							@foreach($product->categories as $category)
								<p class="text-primary">{{ $category->name }}</p>
							@endforeach
							</td>
							<td>
							@foreach($product->attributes->unique('id') as $attribute)
								<p class="text-primary">{{ $attribute->name }}</p>
							@endforeach
							</td>
							<td>{{ $product->price }}</td>
							<td>{{ $product->quantity }}</td>
							<td>{{ $product->stock_status==1? "stock" : "out-of-stock" }}</td>
							<td>
								<a href="{{ route('admin.products.edit', $product->id) }}" class="btn btn-primary btn-xs"><i class="material-icons">edit</i></a>
								@if($product->trashed())
								<form action="{{ route('admin.products.restore', $product->id) }}" method="get">
								@csrf
									<button class="btn btn-warning btn-xs"><i class="material-icons">restore</i></button>
								</form>
								@else
								<form action="{{ route('admin.products.destroy', $product->id) }}" method="post">
								@csrf
								@method('DELETE')
									<button class="btn btn-danger btn-xs"><i class="material-icons">delete</i></button>
								</form>
								@endif
								
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				{{ $products->links() }}
				</div>
			</div>
		  	<!-- End .table-responsive -->

			<!-- Pagination -->
		  	<div class="row">
		  		<div class="text-center">
		  			
		  		</div>
		  	</div>
		</div>
		<!-- End .tab-pane -->
		<!-- End Show Product -->

		<!-- Create Product -->
		<div role="tabpanel2" class="tab-pane fade" id="create" style="padding: 20px;">
			<form action="{{ route('admin.products.store') }}" id="form_validation" method="post" enctype="multipart/form-data">
			@csrf
				<div class="clone-parent">
					<div class="form-group">
						<label class="control-label">Product photos (can attach more than one): </label>
					</div>

					<div class="row">
						<div class="form-group col-sm-2 imgUp">
							<img src="http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg" class="imagePreview">
								<label class="btn btn-primary upload">
									Upload<input type="file" name="photos[]" class="uploadFile img" style="width: 0px;height: 0px;overflow: hidden;">
								</label>
						</div>
  						<i class="fa fa-plus imgAdd"></i>
					</div>
					

					<div class="row row-clone">
						<div class="form-group col-sm-12">
							<div class="form-line">
								<label for="name">Name</label>
								<input type="text" id="name" name="name" class="form-control" >
							</div>
						</div>
					</div>

					<div class="row row-clone">
						<div class="form-group col-sm-12">
							<div class="form-line">
								<label for="sku">SKU</label>
								<input type="text" id="sku" name="sku" class="form-control">
							</div>
						</div>
					</div>

					<div class="row row-clone">
						<div class="form-group col-sm-12">
							<div class="form-line">
								<label for="description">Description</label>
								<textarea name="description" id="" cols="50" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>

					<div class="row row-clone">
						<div class="form-group col-sm-12">
							<div class="form-line">
								<label for="quantity">Quantity</label>
								<input type="number" class="form-control" id="quantity" name="quantity" min=0 >
							</div>
						</div>
					</div>

					<div class="row row-clone">
						<div class="form-group col-sm-6">
							<div class="form-line">
								<label for="category">Select Categories</label>
								<select id="category" name="category_id[]" class="selectpicker form-control" data-live-search="true" data-live-search-placeholder="Search Category" multiple>
									@foreach($categories as $category)
										<option value="{{ $category->id }}"  >{{ $category->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					
						<div class="form-group col-sm-6">
							<div class="form-line">
								<label for="supplier">Select Supplier</label>
								<select id="supplier" name="supplier_id" class="selectpicker form-control" data-live-search="true" data-live-search-placeholder="Search Supplier">
									<option value="">Select supplier</option>
									@foreach($suppliers as $supplier)
										<option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>

					<div class="row row-clone">
						<div class="form-group col-sm-8">
							<div class="form-line">
								<label for="price">Price</label>
								<input type="text" class="form-control" id="price" name="price">
							</div>
						</div>

						<div class="form-group col-sm-4">
							<div class="form-line">
								<label for="currency">Currency</label>
								<select id="currency" name="currency" class="form-control" >
									@foreach(config('constant.currency') as $key => $value)
										<option value="{{ $key }}">{{ $value }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>

					<div class="row row-clone" id="showthis">
						<div class="form-group col-sm-6"> 
							<label>Discount Price</label>
							<input type="text" name="dprice" class="form-control" >
						</div>

						<div class="form-group col-sm-3">
							<label>Discount From</label>
							<input type="date" name="fromdate" class="form-control" >
						</div>

						<div class="form-group col-sm-3">
							<label>Discount To</label>
							<input type="date" name="todate" class="form-control" >
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12">	
							<table id="newtable" class="table table-responsive">
								<tr><label>Select Attribute</label></tr>
								<tr data-id="1">
									<td class="col-sm-6">
										<select class="form-control attribute_type" id="attribute_type_1" name="attribute_id[1]">
											<option value="" disabled selected>Select Attribute</option>
											@foreach($attributes as $attribute)
											<option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
											@endforeach
										</select>
									</td>
									<td class="col-sm-5">
									<select id="attribute_value_1" name="attribute_value[1]" class="form-control">
									</select>
									</td>
									<td class="col-sm-1">
										<button type="button" name="add" id="addButton" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
									</td>
								</tr>
							</table>
						</div>
					</div>

					<br><br>
					<div class="row clearfix">
						<input type="submit" class="btn btn-success" value="Add Product" style="float:right;">
					</div>
				</div>
			</form>
		</div>			
	</div>
@stop
@section('scripts')
<script>
	$(document).ready(function(){
		// form validation jquery
		$('#form_validation').validate({
			rules: {
				name: {
					required: true
				},

				description: {
					required: true
				},

				price: {
					required: true
				},

				currency: {
					required: true
				},

				'category_id[]': {
					required: true
				},

				'photos[]': {
					required: true,
					extension: "jpg|jpeg|png"
				}
			},
			errorPlacement: function(error, element) {
				$(element).parents('.form-group').append(error);
				error.css('color', '#f56042');
				
  			}
		});

		//multiple image upload css-js
		$(".imgAdd").click(function(){
			$(this).closest(".row").find('.imgAdd').before('<div class="col-sm-2 imgUp">\
																<img src="http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg" class="imagePreview">\
																	<label class="btn btn-primary upload">Upload<input type="file" name="photos[]" class="uploadFile img" value="Upload Photo" style="width:0px;height:0px;overflow:hidden;"></label>\
																	<i class="fa fa-times del"></i></div>');
		});

		//preview img remove
		$(document).on("click", "i.del" , function() {
			$(this).parent().remove();
		});

		//change new file
		$(function() {
			$(document).on("change",".uploadFile", function(e)
			{
				// var fileName = e.target.files[0].files;console.log(fileName);
				var uploadFile = $(this);
				var files = !!this.files ? this.files : [];
				if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
		
				if (/^image/.test( files[0].type)){ // only image file
					var reader = new FileReader(); // instance of the FileReader
					reader.readAsDataURL(files[0]); // read the local file

					reader.onloadend = function(){ // set image data as background of div
						//alert(uploadFile.closest(".upimage").find('.imagePreview').length);
						// uploadFile.closest(".imgUp").find('.imagePreview').css("background-image", "url("+this.result+")");
						uploadFile.closest(".imgUp").find('.imagePreview').attr('src', this.result);
					}
				}
			
			});
		});

		//dynamically add/remove product_attribute form
        var i = 1;
        $('#addButton').on('click', function(){
            i++;
            // $('#newtable').append('<tr id="row'+i+'" class="dynamic_added"><td><select name="gender" class="form-control"><option value="">Select Attribute</option><option>Male</option><option>Female</option></select></td><td><select id="attrSelectPicker" class="form-control selectpicker" multiple><option value="">Select Attribute</option><option>Male</option><option>Female</option></select></td><td><button type="button" name="remove" id="'+i+'"  class="btn btn-danger btn_remove">X</button></td></tr>');
            $('#newtable').append('<tr data-id="'+i+'" id="row'+i+'">\
									<td>\
										<select class="form-control attribute_type" id="attribute_type_'+i+'" name="attribute_id['+i+']">\
											<option value="" disabled selected>Select Attribute</option>\
											@foreach($attributes as $attribute)\
											<option value="{{ $attribute->id }}">{{ $attribute->name }}</option>\
											@endforeach\
										</select>\
									</td>\
									<td>\
									<select id="attribute_value_'+i+'" name="attribute_value['+i+']" class="form-control">\
									</select>\
									</td>\
									<td>\
										<button type="button" id="'+i+'" name="btn_remove" class="btn btn-danger btn_remove"><span class="glyphicon glyphicon-minus"></span></button>\
									</td>\
								</tr>');
            // $('#selectValue').selectpicker();
        });

        $(document).on('click', '.btn_remove', function(){ 
			$(this).closest('tr').remove();
		});

		//dynamically dropdown select
		$(document).on('change', '.attribute_type', function(){  
			//duplicate select option 
			var length = 0;
			var first = $(this).children("option:selected").val(); console.log(first);

			$('.attribute_type').each(function(){
				var second = $(this).val(); console.log(second);
				if(first == second){
					length++;
				}
			});

			if(length > 1){
				$(this).val('');
				alert('You have already selected this option previously - please choose another.')
			}

			//ajax dynamic dropdown
			var id = $(this).closest('tr').data("id"); //console.log(id);
			var attr_id = $(this).val(); //console.log(attr_id);
            // alert(id);
			var div = $(this).parent();
			var op = " ";

			$.ajax({
				type: 'get',
				url: "{{url('/admin/products/getAjaxValue')}}?id="+attr_id,
				data: {'id':attr_id},
				success:function(data){
					// console.log(data[0].name);
					$('#attribute_value_'+id).empty();
					op += '<option value="" disabled selected>Select Attribute Value</option>';
					for(var j=0; j<data.length; j++){
						op += '<option value="'+data[j].id+'">'+data[j].name+'</option>';
					}
					console.log(op);
					div.find('#attribute_value').html(' ');
					// console.log(s);
					$('#attribute_value_'+id).append(op);
					// console.log(o);
				},
				error:function(){
					console.log('fail');
				}
			});
			$('#attribute_value').empty();
		});

		// keyword search autocomplete
		$('.name').on('keyup', function(){
			var query = $(this).val();
			console.log(query);
			var output = '';
			if(query.length > 0){
				$.ajax({
					url:"{{ route('admin.products.autocomplete') }}",
					type: 'get',
					data: {'name': query},
					success: function(data){
						output += '<ul class="list-group" style="position: absolute; width: 100%; max-height: 160px; overflow: auto;  z-index: 1">';
						for(var n=0; n<data.length; n++){
							output += '<li class="list-group-item">'+data[n].name+'</li>';
						}
						output += '</ul>';
						$('#name_list').html(output);
					}
				});
			}else{
				$('#name_list').html('');
			}	
		});

		$(document).on('click', 'li', function(){
			var name = $(this).text();
			$('#keyword').val(name);
			$('#name_list').html('');	
		});

		//clear search 
        $(document).on('click', '.clear_search', function(){
			$('#keyword').val('');
			$('#category').val('');
			$('#attribute').val('');
			$('#qty').val('');
		});
	});
</script>
@stop