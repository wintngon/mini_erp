<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Attribute;
use App\AttributeValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AttributeValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $values = AttributeValue::withTrashed()->where('attribute_id', $id)->paginate(10);
        $attribute = Attribute::find($id);

        return view('admin.attribute-values.index', compact('values', 'attribute'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|unique:attribute_value,name'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.attribute-values', $data['attribute_id'])
                        ->withErrors($validator)
                        ->withInput();
        }

        DB::beginTransaction();

        try {
            $attr_value = new AttributeValue();
            $attr_value->attribute_id = $data['attribute_id'];
            $attr_value->name = $data['name'];
            $attr_value->save();

            DB::commit();

            Session::flash('created_message', 'The attribute-value has been created');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('error_message', 'Failed !! The attribute-value has not been created');
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $attr_id)
    {
        $value = AttributeValue::find($id);
        $attribute = Attribute::find($attr_id);
        if($value != null){
            return view('admin.attribute-values.edit', compact('value','attribute'));
        }else{
            Session::flash('error_message', 'The attribute value is deleted. So it cannot be edited! ');
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|unique:attribute_value,name,'.$id,
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.attribute-values.edit', ['id'=>$id,'attr_id'=>$data['attribute_id']])
                        ->withErrors($validator)
                        ->withInput();
        }

        $value = AttributeValue::find($id);
        $value->attribute_id = $data['attribute_id'];
        $value->name = $data['name'];
        $value->update();

        $values = AttributeValue::where('attribute_id', $data['attribute_id'])->get();
        $attribute = Attribute::find($data['attribute_id']);

        Session::flash('updated_message', 'The attribute value has been updated');
        return view('admin.attribute-values.index', compact('values', 'attribute'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $attr_id)
    {
        // dd($attr_id);
        $val = AttributeValue::find($id);
        $val->delete();

        Session::flash('deleted_message', 'The attribute value has been deleted');
        return redirect()->route('admin.attribute-values', $attr_id);
    }

    public function restore($id) 
    {
        $value = AttributeValue::withTrashed()->find($id)->restore();
        return redirect()->back();
    }
}
