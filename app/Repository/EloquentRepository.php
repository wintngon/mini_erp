<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

class EloquentRepository
{
    protected $model;

    public function __construct(Model $model){
        $this->model = $model;
    }

    public function all(){
        return $this->model->get();
    }

    public function create(array $data){
        return $this->model->create($data);
    }

    public function edit($id){
        return $this->model->find($id);
    }

    public function show($id){
        return $this->model->findOrFail($id);
    }

    public function update(array $data, $id){
        $record = $this->find($id);
        return $record->update($data);
    }

    public function destroy($id){
        return $this->model->destroy($id);
    }
}
