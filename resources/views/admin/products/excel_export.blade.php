<div>
    <table>
        <thead>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Description</th>
				<th>Category</th>
				<th>Attribute</th>
				<th>Price</th>
				<th>Qty</th>
				<th>Stock-status</th>
			</tr>
		</thead>
        <tbody>	
			@foreach($products as $product)
			<tr>
				<td>{{ $product->id }}</td>
				<td>{{ $product->name }}</td>
				<td>{{ $product->description }}</td>
				<td>
					@foreach($product->categories as $category)
						<p class="text-primary">{{ $category->name }}</p>
					@endforeach
				</td>
				<td>
					@foreach($product->attributes as $attribute)
						<p class="text-primary">{{ $attribute->name }}</p>
					@endforeach
				</td>
				<td>{{ $product->price }}</td>
				<td>{{ $product->quantity }}</td>
				<td>{{ $product->stock_status==1? "stock" : "out-of-stock" }}</td>
			</tr>
			@endforeach
		</tbody>
    </table>
</div>
				