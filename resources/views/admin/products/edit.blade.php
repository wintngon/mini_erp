@extends('layouts.admin')

@section('content')

	<h1>Production</h1>

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

    <ul class="nav nav-tabs">
		<li role="presenstation" class="active"><a href="#edit"><strong>Edit Product</strong></a></li>
	</ul>

    <div class="container" style="padding: 20px;">
        <form action="{{ route('admin.products.update', $product->id) }}" id="form_validation" method="post" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
            <div class="clone-parent">
				<div class="form-group">
					<label class="control-label">Product photos (can attach more than one): </label>
				</div>

                <div class="row row-clone">
				@for($j=0; $j<count($images); $j++)
					<div class="col-sm-2 imgUp" id="{{$j}}" >
    					<img src="{{ url('uploads/'.$images[$j]->file_name.'.'.$images[$j]->file_extension) }}" class="imagePreview">
							<label class="btn btn-primary upload">
								Upload<input type="file" name="photos[{{$j}}]" class="uploadFile img" style="width: 0px;height: 0px;overflow: hidden;">
								<input type="hidden" name="photos[{{$j}}]" value="{{ $images[$j]->id }}">
							</label>
							<i class="fa fa-times del" id="{{$j}}"></i>
  					</div>
				@endfor
				<i class="fa fa-plus imgAdd"></i>
				</div>

                <div class="row row-clone">
                    <div class="form-group col-sm-12">
                        <div class="form-line">
                            <label for="name">Name</label>
                            <input type="text" id="name" name="name" value="{{ $product->name }}" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row row-clone">
						<div class="form-group col-sm-12">
							<div class="form-line">
								<label for="sku">SKU</label>
								<input type="text" id="sku" name="sku" value="{{ $product->SKU }}" class="form-control">
							</div>
						</div>
					</div>

                <div class="row row-clone">
                    <div class="form-group col-sm-12">
                        <div class="form-line">
                            <label for="description">Description</label>
                            <textarea name="description" id="" cols="50" rows="5" class="form-control">{{ $product->description }}</textarea>
                        </div>
                    </div>
                </div>

                <div class="row row-clone">
                    <div class="form-group col-sm-12">
                        <div class="form-line">
                            <label for="quantity">Quantity</label>
                            <input type="number" class="form-control" id="quantity" name="quantity" value="{{ $product->quantity }}" min=0>
                        </div>
                    </div>
                </div>

                <div class="row row-clone">
                    <div class="form-group col-sm-6">
                        <div class="form-line">
                            <label for="category">Select Categories</label>
                            <select class="selectpicker form-control" name="category_id[]" id="category" data-live-search="true" multiple>
                                @foreach ($categories as $key => $value)
                                    @if(array_key_exists($key, $category_product))
                                    <option value="{{ $key }}" selected>{{ $value }}</option>
                                    @else
                                    <option value="{{ $key }}">{{ $value }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-sm-6">
						<div class="form-line">
							<label for="supplier">Select Supplier</label>
							<select id="supplier" name="supplier_id" class="selectpicker form-control" data-live-search="true" data-live-search-placeholder="Search Supplier">
								<option value="">Select supplier</option>
								@foreach($suppliers as $key => $value)
									<option value="{{ $key }}" {{ $key == $product->supplier['id'] ? 'selected' : '' }}>{{ $value }}</option>
								@endforeach
							</select>
						</div>
					</div>
                </div>

                <div class="row row-clone">
                    <div class="form-group col-sm-8">
                        <div class="form-line">
                            <label for="price">Price</label>
                            <input type="text" class="form-control" id="price" name="price" value="{{ $product->price }}">
                        </div>
                    </div>

                    <div class="form-group col-sm-4">
						<div class="form-line">
							<label for="currency">Currency</label>
							<select id="currency" name="currency" class="form-control" >
								@foreach(config('constant.currency') as $key => $value)
                                    <option value="{{ $key }}" {{ $key == $product->currency ?'selected':'' }}>{{ $value }}</option>
								@endforeach
							</select>
						</div>
					</div>
                </div>

                <div class="row row-clone" id="showthis">
					<div class="form-group col-sm-6"> 
						<label>Discount Price</label>
						<input type="text" name="dprice" class="form-control" value="{{ $product->discount_price }}">
					</div>

					<div class="form-group col-sm-3">
						<label>Discount From</label>
						<input type="date" name="fromdate" class="form-control" value="{{ ($product->discount_from != null) ? Carbon\Carbon::parse($product->discount_from)->toDateString() : null }}">
					</div>

					<div class="form-group col-sm-3">
						<label>Discount To</label>
						<input type="date" name="todate" class="form-control" value="{{ ($product->discount_from != null) ? Carbon\Carbon::parse($product->discount_to)->toDateString() : null }}">
					</div>
				</div>

                <div class="row">
					<div class="col-sm-12">	
						<table id="newtable" class="table table-responsive">
						<tr><label>Select Attribute</label></tr>
                        @if(count($product->attributes) > 0)
                            @for($i=0; $i<count($product->attributes); $i++)
                            <tr data-id="{{$i}}">
								<td class="col-sm-6">
									<select class="form-control attribute_type" id="attribute_type_{{$i}}" name="attribute_id[{{$i}}]">
									<option value="" disabled selected>Select Attribute</option>
                                    @foreach($attributes as $attribute)
                                        @if($data[$i]['attribute_id'] == $attribute->id)
                                        <option value="{{$attribute->id}}" selected="">{{$attribute->name}}</option>
                                        @else
                                        <option value="{{$attribute->id}}">{{$attribute->name}}</option>
                                        @endif
                                    @endforeach
									</select>
								</td>
								<td class="col-sm-5">
	    							<select id="attribute_value_{{$i}}" name="attribute_value[{{$i}}]" class="form-control">
                                    @foreach($data[$i]['attributeValue'] as $attr_val)
                                        @if($data[$i]['value'] == $attr_val->id)
                                        <option value="{{$attr_val->id}}" selected>{{$attr_val->name}}</option>
                                        @else
                                        <option value="{{$attr_val->id}}">{{$attr_val->name}}</option>
                                        @endif
                                    @endforeach
									</select>
								</td>
								<td class="col-sm-1">
                                @if($i+1 == 1)
                                    <button type="button" name="add" id="addButton" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>    
                                @else 
                                    <button type="button" id="'+i+'" name="btn_remove" class="btn btn-danger btn_remove"><span class="glyphicon glyphicon-minus"></span></button>
                                @endif
								</td>
							</tr>
                            @endfor
                        @else
							<tr><label>Select Attribute</label></tr>
                            <tr data-id="1">
								<td class="col-sm-6">
									<select class="form-control attribute_type" id="attribute_type_1" name="attribute_id[1]">
										<option value="" disabled selected>Select Attribute</option>
										@foreach($attributes as $attribute)
										<option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
										@endforeach
									</select>
		    					</td>
								<td class="col-sm-5">
                                    <select id="attribute_value_1" name="attribute_value[1]" class="form-control">
                                    </select>
								</td>
								<td class="col-sm-1">
									<button type="button" name="add" id="addButton" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
							    </td>
							</tr>
                        @endif
						</table>
					</div>
				</div>

                <br><br>
                <div class="row clearfix">
                    <input type="submit" class="btn btn-success" value="Update Product" style="float:right;">
                </div>
            </div>
        </form>
    </div>

@stop

@section('scripts')
<script>
	$(document).ready(function(){
		$('#form_validation').validate({
			rules: {
				name: {
					required: true
				},

				description: {
					required: true
				},

				price: {
					required: true
				},

				currency: {
					required: true
				},

				'category_id[]': {
					required: true
				},

				'photos[]': {
					required: true,
					extension: "jpg|jpeg|png|gif"
				}
			},
			errorPlacement: function(error, element) {
				$(element).parents('.form-group').append(error);
				error.css('color', '#f56042');
  			}
		}); 

		//multiple image upload css-js
		$(".imgAdd").click(function(){
			// var id = $(".imgUp:last").attr("id"); console.log(id);
			// id++;
			$(this).closest(".row").find('.imgAdd').before('<div class="col-sm-2 imgUp">\
																<img src="http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg" class="imagePreview">\
																	<label class="btn btn-primary upload">Upload<input type="file" name="photos[]" class="uploadFile img" value="Upload Photo" style="width:0px;height:0px;overflow:hidden;"></label>\
																	<i class="fa fa-times del"></i></div>');
		});

		//preview image remove
		$(document).on("click", "i.del" , function() {
			$(this).parent().remove();
		});

		//change new file
		$(function() {
			$(document).on("change",".uploadFile", function()
			{
				var uploadFile = $(this); //console.log(uploadFile);
				var files = !!this.files ? this.files : [];
				if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
		
				if (/^image/.test(files[0].type)){ // only image file
					var reader = new FileReader(); // instance of the FileReader
					reader.readAsDataURL(files[0]); // read the local file
		
					reader.onloadend = function(){ 
						// uploadFile.closest(".imgUp").find('.imagePreview').css("background-image", "url("+this.result+")");
						uploadFile.closest(".imgUp").find('.imagePreview').attr('src', this.result);
					}
				}
			});
		});	

		//dynamically add/remove product_attribute form
        var i = 1;
        $('#addButton').on('click', function(){
            i++;
            $('#newtable').append('<tr data-id="'+i+'" id="row'+i+'">\
									<td>\
										<select class="form-control attribute_type" id="attribute_type_'+i+'" name="attribute_id['+i+']">\
											<option value="" disabled selected>Select Attribute</option>\
											@foreach($attributes as $attribute)\
											<option value="{{ $attribute->id }}">{{ $attribute->name }}</option>\
											@endforeach\
										</select>\
									</td>\
									<td>\
									<select id="attribute_value_'+i+'" name="attribute_value['+i+']" class="form-control">\
									</select>\
									</td>\
									<td>\
										<button type="button" id="'+i+'" name="btn_remove" class="btn btn-danger btn_remove"><span class="glyphicon glyphicon-minus"></span></button>\
									</td>\
								</tr>');
            // $('#selectValue').selectpicker();
        });

        $(document).on('click', '.btn_remove', function(){  
			$(this).closest('tr').remove();
		}); 

		//dynamically dropdown for attribute type
		$(document).on('change', '.attribute_type', function(){
			//duplicate select option 
			var length = 0;
			var first = $(this).children("option:selected").val(); //console.log(first);
			$('.attribute_type').each(function(){
				var second = $(this).val(); //console.log(second);
				if(first == second){
					length++;
				}
			});

			if(length > 1){
				$(this).val('');
				alert('You have already selected this option previously - please choose another.')
			}

			//ajax dynamic dropdown
			var id = $(this).closest('tr').data("id"); //console.log(id);
			var attr_id = $(this).val(); //console.log(attr_id);
            // alert(id);
			var div = $(this).parent();
			var op = " ";

			$.ajax({
				type: 'get',
				url: "{{url('/admin/products/getAjaxValue')}}?id="+attr_id,
				data: {'id':attr_id},
				success:function(data){
					// console.log(data[0].name);
                    $('#attribute_value_'+id).empty();
					op += '<option value="" disabled selected>Select Attribute Value</option>';
					for(var j=0; j<data.length; j++){
						op += '<option value="'+data[j].id+'">'+data[j].name+'</option>';
					}
					// console.log(op);
					div.find('#attribute_value').html(' ');
					// console.log(s);
					$('#attribute_value_'+id).append(op);
					// console.log(o);
				},
				error:function(){
					console.log('fail');
				}
			});
			$('#attribute_value').empty();
		});
	});
</script>
@stop