<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\APIBaseController as APIBaseController;

class CategoryController extends APIBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate(10);
        return view('admin.categories.index', compact('categories'));

        // return $this->sendResponse($categories->toArray(), "Categories retrieved successfully");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|unique:categories'
        ]);

        if ($validator->fails()) {
            return redirect('admin/categories')
                        ->withErrors($validator)
                        ->withInput();

            // return $this->sendError('Validation Error.', $validator->errors());
        }

        DB::beginTransaction();

        try{
            $category = new Category();
            $category->name = $data['name'];
            $category->save();
            DB::commit();

            Session::flash('created_message', 'The category has been created');
            return redirect()->back();
            // return $this->sendResponse($category->toArray(), 'Category is successfully created');
        }catch(\Exception $e){
            DB::rollback();
            Session::flash('error_message', 'Failed !! The category has not been created');
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $category = Category::find($id);
        $categories = Category::get();
        return view('admin.categories.edit', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|unique:categories,name,'.$id
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            // return $this->sendError('Validation Error.', $validator->errors());
        }

        $category = Category::find($id);
        if (is_null($category)) {
            return $this->sendError('Category not found.');
        }
        $category->name = $data['name'];
        $category->update();

        Session::flash('updated_message', 'The category has been updated');
        return redirect('admin/categories');
        // return $this->sendResponse($category->toArray(), 'Category is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        Session::flash('deleted_message', 'The category has been deleted');
        return redirect('admin/categories');
        // return $this->sendResponse($id, 'Category deleted successfully.');
    }
}
