@extends('layouts.admin')

@section('content')

{{ Breadcrumbs::render('categories', $categories) }}

	<h1>Category</h1>

    @include('includes.message')

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

    <ul class="nav nav-tabs">
		<li role="presenstation" class="active"><a href="#"><strong>View / Create</strong></a></li>
	</ul>

    <div class="container " style="padding: 10px;">
        
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Category Lists
                </div>

                <div class="panel-body">
                    <!-- Category List -->
                    <table class="table table-striped text-center">
                        <thead">
                            <th class="text-center">ID</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Actions</th>
                        </thead>

                        <tbody>
                            @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->name }}</td>
                                <td>
                                <a href="{{ route('admin.categories.edit', $category->id) }}" class="btn btn-primary btn-xs"><i class="material-icons">edit</i></a>
                                <form action="{{ route('admin.categories.destroy', $category->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                    <button class="btn btn-danger btn-xs"><i class="material-icons">delete</i></button>
                                </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $categories->links() }}
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Category
                </div>

                <div class="panel-body">
                    <!-- New Category -->
                    <form action="{{ route('admin.categories.store') }}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="name" class="control-label">Category</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                        </div>

                        <div class="row clearfix" style="float:right;">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-plus"></i> Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
