@extends('layouts.admin')

@section('content')

{{ Breadcrumbs::render('customers', $customers) }}
	<h1>Customers</h1>

	@include('includes.message')

	@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

	<!-- Start .nav nav-tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presenstation" class="active"><a href="#view" aria-controls="view" role="tab" data-toggle="tab"><strong>View Customers</strong></a></li>
		<li role="presenstation"><a href="#create" aria-controls="create" role="tab" data-toggle="tab"><strong>Create Customer</strong></a></li>
	</ul>
    <!-- End .nav nav-tabs -->

    <div class="tab-content">
		<!-- Show Products -->
		<div role="tabpanel1" class="tab-pane fade in active" id="view">
			<div class="row">
				<div class="table-responsive col-md-12">
				<table class="table table-bordered ">
					<thead>
					<tr>
                        <th>Id</th>
                        <th>Name</th>
						<th>Address</th>
                        <th>Phone</th>
                        <th>Created At</th>
                        <th>Actions</th>
					</tr>
					</thead>
					<tbody>	
                    @foreach($customers as $customer)
                    <tr>
                        <td>{{$customer->id}}</td>
                        <td>{{$customer->name}}</td>
                        <td>{{$customer->address1}}</td>
                        <td>{{$customer->phone}}</td>
                        <td>{{ Carbon\Carbon::parse($customer->created_at)->format('d-m-Y') }}</td>
                        <td>
                        <a href="{{ route('admin.customers.edit', $customer->id) }}" class="btn btn-primary btn-xs"><i class="material-icons">edit</i></a>

                        @if($customer->trashed())
                            <form action="{{ route('admin.customers.restore', $customer->id) }}" method="get">
                            @csrf
                                <button class="btn btn-warning btn-xs"><i class="material-icons">restore</i></button>
                            </form>
                        @else
                            <form action="{{ route('admin.customers.destroy', $customer->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                                <button class="btn btn-danger btn-xs"><i class="material-icons">delete</i></button>
                            </form>
                        @endif
                        </td>
                    </tr>
                    @endforeach
					</tbody>
				</table>
				
				</div>
			</div>
		  	<!-- End .table-responsive -->

			<!-- Pagination -->
		  	<div class="row">
		  		<div class="text-center">
		  			{{$customers->links()}}
		  		</div>
		  	</div>
		</div>
		<!-- End .tab-pane -->
		<!-- End Show Product -->

		<!-- Create Product -->
        <div role="tabpanel2" class="tab-pane fade" id="create" style="padding: 20px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Customer
                </div>

                <div class="panel-body">
                    <form action="{{ route('admin.customers.store') }}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="name" class="control-label">Name</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="phone" class="control-label">Phone</label>
                                <input type="text" name="phone" id="phone" class="form-control">
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="address1" class="control-label">Address 1</label>
                                <textarea name="address1" id="address1" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="address2" class="control-label">Address 2</label>
                                <textarea name="address2" id="address2" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="row clearfix" style="float:right;">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-plus"></i> Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
		</div>			
	</div>
    
@stop