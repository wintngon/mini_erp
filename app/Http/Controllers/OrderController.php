<?php

namespace App\Http\Controllers;

use DB;
use Excel;
use Validator;
use App\Media;
use App\Order;
use App\Product;
use App\Customer;
use App\OrderProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    public function search(Request $request){   //dd($request);
        $customers = Customer::get();

        $query = Order::select('orders.*');

        if($request->name != ''){
            $customer = Customer::where('name', $request->name)->pluck('id')->toArray();
            $query->whereIn('customer_id', $customer);
        }

        if($request->order_no != ''){
            $query->where('order_no', 'LIKE', '%'.$request->order_no.'%')->get();
        }

        if($request->channel != ''){
            $query->where('channel', $request->channel)->get();
        }

        if($request->status != ''){
            $query->where('status', $request->status)->get();
        }

        if($request->payment_method != ''){
            $query->where('payment_method', $request->payment_method)->get();
        }

        if($request->export == 'export'){
            return $this->exportBySearchQuery($query);
        }

        if($request->clear == 'clear'){
            return $this->index();
        }

        $orders = $query->withTrashed()->orderBy('created_at', 'desc')->paginate(6);

        return view('admin.orders.index', compact('orders', 'customers'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::withTrashed()->orderBy('created_at', 'desc')->paginate(6);
        $customers = Customer::get();

        return view('admin.orders.index', compact('orders', 'customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        // $validator = Validator::make($data, [
        //     'order_no' => 'required|unique:orders',
        //     'channel' => 'required',
        //     'status' => 'required',
        //     'customer_id' => 'required',
        //     'payment_method' => 'required',
        //     // 'total_amount' => 'required',
        //     'product' => 'required',
        //     'product.*' => 'required'
        // ]);

        // if ($validator->fails()) {
        //     return redirect('admin/orders')
        //                 ->withErrors($validator)
        //                 ->withInput();
        // }

        DB::beginTransaction();

        try {
            $order = Order::create([
                'order_no' => $data['order_no'],
                'channel' => $data['channel'],
                'status' => $data['status'],
                'customer_id' => $data['customer_id'],
                'payment_method' => $data['payment_method'],
                'total_amount' => $data['total_amount']
            ]);
            // dd($data['discount_price']);
            if($data['product']){
                $order_product = [];

                foreach($data['product'] as $key => $name){
                    $product_id[] = Product::where('name', $name)->value('id');
                }
                // dd($product_id);
    
                foreach($data['regular_price'] as $key => $price){
                    $regular_price[] = $price;
                }
                // dd($regular_price);
    
                foreach($data['qty'] as $key => $quantity){
                    $qty[] = $quantity;
                }
                // dd($qty);
    
                foreach($data['discount_price'] as $key => $discount_price){
                    $dprice[] = $discount_price;
                }
                // dd($dprice);
    
                foreach($data['amount'] as $key => $amount){
                    $total_amount[] = $amount;
                }
                // dd($total_amount);
    
                for($i = 0; $i<count($data['product']); $i++){
                    $order_product[$i] = [
                        'product_id' => $product_id[$i],
                        'price' => $regular_price[$i],
                        'quantity' => $qty[$i],
                        'discount_price' => ($dprice[$i] != '' && $dprice[$i] != null)? $dprice[$i]: null,
                        'total_amount' => $total_amount[$i]
                    ];
                }
                // dd($product_id);
                // dd($order_product);
                $order->products()->attach($order_product);
            }
            // dd($order_product);

            DB::commit();

            Session::flash('created_message', 'The order has been created');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            Session::flash('error_message', 'Failed !! The order has not been created');
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        if($order != null){
            $customers = Customer::get();
            $order_product = OrderProduct::where('order_id', $order->id)->get();

            $pivotData = [];
            foreach($order_product as $item){
                $pivotData[] = [
                    'media' => Media::where('product_id', $item->product_id)->first(),
                    'product_id' => $item->product_id,
                    'product' => Product::where('id', $item->product_id)->value('name'),
                    'regular_price' => $item->price,
                    'quantity' => $item->quantity,
                    'discount_price' => $item->discount_price,
                    'amount' => $item->total_amount
                ];
            }
            // dd($pivotData);

            // $productData = [];
            foreach($order_product as $item){
                $media = Media::where('product_id', $item->product_id)->first();
            }
            // dd($media);

            return view('admin.orders.edit', compact('order', 'customers', 'order_product', 'pivotData'));
        }else{
            Session::flash('error_message', 'This order is deleted. So it cannot be edited! ');
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        // $validator = Validator::make($data, [
        //     'order_no' => 'required|unique:orders,order_no,'.$id,
        //     'channel' => 'required',
        //     'status' => 'required',
        //     'customer_id' => 'required',
        //     'payment_method' => 'required',
        //     'total_amount' => 'required',
        //     'product' => 'required',
        //     'product.*' => 'required'
        // ]);

        // if ($validator->fails()) {
        //     return redirect()->back()
        //                 ->withErrors($validator)
        //                 ->withInput();
        // }

        $order = Order::find($id);
        $order->order_no = $data['order_no'];
        $order->channel = $data['channel'];
        $order->status = $data['status'];
        $order->customer_id = $data['customer_id'];
        $order->payment_method = $data['payment_method'];
        $order->total_amount = $data['total_amount'];
        $order->update();

        if($data['product']){
            $order->products()->detach();
            $order_product = [];

            foreach($data['product'] as $key => $name){
                $product_id[] = Product::where('name', $name)->value('id');
            }
            // dd($product_id);

            foreach($data['regular_price'] as $key => $price){
                $regular_price[] = $price;
            }
            // dd($regular_price);

            foreach($data['qty'] as $key => $quantity){
                $qty[] = $quantity;
            }
            // dd($qty);

            foreach($data['discount_price'] as $key => $discount_price){
                $dprice[] = $discount_price;
            }
            // dd($dprice);

            foreach($data['amount'] as $key => $amount){
                $total_amount[] = $amount;
            }

            for($i = 0; $i<count($data['product']); $i++){
                $order_product[$i] = [
                    'product_id' => $product_id[$i],
                    'price' => $regular_price[$i],
                    'quantity' => $qty[$i],
                    'discount_price' => ($dprice[$i] != '' && $dprice[$i] != null)? $dprice[$i]: null,
                    'total_amount' => $total_amount[$i]
                ];
            }
            // dd($order_product);
            $order->products()->attach($order_product);
        }

        Session::flash('updated_message', 'The order has been updated');
        return redirect('admin/orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        // $order->customer()->detach();
        // $order->products()->detach();
        $order->delete();

        Session::flash('deleted_message', 'The order has been deleted');
        return redirect('admin/orders');
    }

    public function restore($id) 
    {
        $order = Order::withTrashed()->find($id)->restore();
        return redirect()->back();
    }

    public function autocomplete_name(Request $request){
        $data = Product::where('name', 'LIKE', '%'.$request->product.'%')->get();

        return response()->json($data);
    }

    public function autocomplete_multiple(Request $request){
        $data = Product::where('name', $request->name)->get();
        // dd($data[0]->id);
        $media = Media::where('product_id', $data[0]->id)->first();
        // dd($media);

        return json_encode(array('val'=>$data, 'media'=>$media));
    }

    public function getCustomerName(Request $request){
        $data = Customer::where('name', 'LIKE', '%'.$request->name.'%')->get();

        return response()->json($data);
    }

    public function getOrderNo(Request $request){
        $data = Order::where('order_no', 'LIKE', '%'.$request->order_no.'%')->get();

        return response()->json($data);
    }

    public function exportBySearchQuery($query){
        $orders = $query->orderBy('created_at', 'desc')->get();

        Excel::create('Order Lists', function($excel) use($orders){
            $excel->sheet('Order Lists', function($sheet) use($orders){
                $sheet->loadView('admin.orders.excel_export', compact('orders'));
            });
        })->export('xlsx');
    }
}
