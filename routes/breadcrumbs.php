<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('admin.'));
});

// Home > Users
Breadcrumbs::for('users', function ($trail) {
    $trail->parent('home');
    $trail->push('Users', route('admin.users.index'));
});

// Home > Products
Breadcrumbs::for('products', function ($trail) {
    $trail->parent('home');
    $trail->push('Products', route('admin.products'));
});

// Home > Categories
Breadcrumbs::for('categories', function ($trail) {
    $trail->parent('home');
    $trail->push('Categories', route('admin.categories.index'));
});

// Home > Suppliers
Breadcrumbs::for('suppliers', function ($trail) {
    $trail->parent('home');
    $trail->push('Suppliers', route('admin.suppliers.index'));
});

// Home > Attributes
Breadcrumbs::for('attributes', function ($trail) {
    $trail->parent('home');
    $trail->push('Attributes', route('admin.attributes.index'));
});

// Home > Attributes > Attribute-value
Breadcrumbs::for('values', function ($trail, $attribute) {
    $trail->parent('attributes');
    $trail->push($attribute->name, route('admin.attribute-values', $attribute));
});

// Home > Customers
Breadcrumbs::for('customers', function ($trail) {
    $trail->parent('home');
    $trail->push('customers', route('admin.customers.index'));
});

// Home > Orders
Breadcrumbs::for('orders', function ($trail) {
    $trail->parent('home');
    $trail->push('orders', route('admin.orders'));
});