<?php

namespace App\Http\Controllers\API;

use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\APIBaseController as APIBaseController;

class AuthController extends APIBaseController
{
    public function register(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|unique:users',
            'role_id' => 'required',
            'password' => 'required|min:6',
            'photo_id' => 'mimes:jpeg,png,bmp'
        ]);

        if($validator->fails()){
            return $this->sendError('Validator error.', $validator->errors());
        }

        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);
        $success = $user->createToken('Myapp')->accessToken;
        // $success['username'] = $user->username;

        return $this->sendResponse($success, 'User register successfully');
    }

    public function login(Request $request){
        // $credential = request(['email', 'password']);
        // if(Auth::attempt($credential)){

        $input = $request->only('email', 'password');
        if(Auth::attempt($input)){ 
            $user = Auth::user();
            $success = $user->createToken('Myapp')->accessToken;

            return $this->sendResponse($success, 'User login successfully');
        }else{
            return $this->sendError('Unauthorised.', ['error' => 'Unauthorised']);
        }
    }

    public function logout(Request $request){
        $isUser = Auth::user()->token()->revoke();

        if($isUser){
            return $this->sendResponse('User logout successfully', 200);
        }else{
            return $this->sendError('Something went wrong.', ['error' => 'Something went wrong']);
        }
    }

    public function getUser(Request $request){
        $user = Auth::user();

        if($user){
            return $this->sendResponse($user->toArray(), 'Profile');
        }else{
            return $this->sendError('User not found');
        }
    }
}
