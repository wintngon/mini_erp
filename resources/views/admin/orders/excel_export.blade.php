<div>
    <table>
        <thead>
			<tr>
                <th>Id</th>
                <th>Order No</th>
				<th>Channel</th>
                <th>Status</th>
                <th>Customer Name</th>
                <th>Payment Method</th>
                <th>Total Amount</th>
			</tr>
		</thead>
        <tbody>	
			@foreach($orders as $order)
			<tr>
				<td>{{ $order->id }}</td>
				<td>{{ $order->order_no }}</td>
				<td>{{ $order->channel }}</td>
				<td>{{ $order->status }}</td>
                <td>
                    {{ $order->customer->name }}
                </td>
                <td>{{ $order->payment_method }}</td>
                <td>{{ $order->total_amount }}</td>
			</tr>
			@endforeach
		</tbody>
    </table>
</div>
				