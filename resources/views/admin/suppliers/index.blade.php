@extends('layouts.admin')

@section('content')

{{ Breadcrumbs::render('suppliers', $suppliers) }}

	<h1>Supplier</h1>

    @include('includes.message')

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

    <!-- Start .nav nav-tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presenstation" class="active"><a href="#view" aria-controls="view" role="tab" data-toggle="tab"><strong>View Suppliers</strong></a></li>
		<li role="presenstation"><a href="#create" aria-controls="create" role="tab" data-toggle="tab"><strong>Create Supplier</strong></a></li>
	</ul>
	<!-- End .nav nav-tabs -->

    <div class="tab-content">
		<!-- Show Suppliers -->
		<div role="tabpanel1" class="tab-pane fade in active" id="view">
            <div class="container">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Supplier Lists
                        </div>

                        <div class="panel-body">
                            <!-- Supplier List -->
                            <table class="table table-striped ">
                                <thead">
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Address1</th>
                                    <th>Phone</th>
                                    <th>Contact Person</th>
                                    <th>Mail</th>
                                    <th>Join Date</th>
                                    <th>Created By</th>
                                    <th>Actions</th>
                                </thead>

                                <tbody>
                                    @foreach($suppliers as $supplier)
                                    <tr>
                                        <td>{{ $supplier->id }}</td>
                                        <td>{{ $supplier->name }}</td>
                                        <td>{{ $supplier->address1 }}</td>
                                        <td>{{ $supplier->phone }}</td>
                                        <td>{{ $supplier->contact_person }}</td>
                                        <td>{{ $supplier->mail }}</td>
                                        <td>{{ $supplier->created_at->diffForHumans() }}</td>
                                        <td>{{ Auth::user()->name }}</td>
                                        <td>
                                            <a href="{{ route('admin.suppliers.edit', $supplier->id) }}" class="btn btn-primary btn-xs"><i class="material-icons">edit</i></a>
                                            <form action="{{ route('admin.suppliers.destroy', $supplier->id) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                                <button class="btn btn-danger btn-xs"><i class="material-icons">delete</i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $suppliers->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End .tab-pane -->
        <!-- End Show Product -->

        <!-- Create Supplier -->
        <div role="tabpanel2" class="tab-pane fade" id="create" style="padding: 20px;">
            <div class="container">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            New Supplier
                        </div>

                        <div class="panel-body">
                            <!-- New Supplier -->
                            <form action="{{ route('admin.suppliers.store') }}" method="POST" class="form-horizontal">
                            {{ csrf_field() }}

                                <div class="row clearfix">
                                    <div class="form-group col-md-12">
                                        <label for="name" class="control-label">Name</label>
                                        <input type="text" name="name" id="name" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="address1" class="control-label">Address 1</label>
                                        <input type="text" name="address1" id="address1" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="address2" class="control-label">Address 2</label>
                                        <input type="text" name="address2" id="address2" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="phone" class="control-label">Phone</label>
                                        <input type="text" name="phone" id="phone" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="contact" class="control-label">Contact Person</label>
                                        <input type="text" name="contact" id="contact" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="email" class="control-label">Mail</label>
                                        <input type="email" name="email" id="email" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="description" class="control-label">Description</label>
                                        <textarea name="description" id="description" cols="30" rows="10" class="form-control"></textarea>
                                    </div>
                                </div>

                                <div class="row clearfix" style="float:right;">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-plus"></i> Add
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End .tab-pane -->
        <!-- End Create Product -->
	</div>
	<!-- End .tab-content -->

@endsection
