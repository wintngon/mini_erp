<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends Model
{
    use SoftDeletes;

    public function products(){
        return $this->belongsToMany('App\Product')->withTimestamps();
    }

    public function values(){
        return $this->hasMany('App\AttributeValue', 'attribute_id', 'id');
    }

    // public static function boot() {
    //     parent::boot();

    //     static::deleting(function($attribute) { // before delete() method call this
    //         $attribute->values()->delete();
    //         // do the rest of the cleanup...
    //     });
    // }
}
