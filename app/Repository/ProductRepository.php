<?php

namespace App\Repository;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class ProductRepository
{
    protected $model;
    public function __construct(Model $model){
        $this->model = $model;
    }

    public function all(){
        return $this->model->get();
    }

    public function store(Request $request){
        // $product = new Product();
        return $this->model->create($request);
    }
}
