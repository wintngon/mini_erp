<?php

namespace App\Http\Controllers;

use DB;
use Log;
use Excel;
use Input;
use Validator;
use App\Media;
use App\Product;
use App\Category;
use App\Supplier;
use App\Attribute;
use App\AttributeValue;
use App\CategoryProduct;
use App\AttributeProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class ProductController extends Controller
{
    public function search(Request $request){
        $categories = Category::get();
        $suppliers = Supplier::get();
        $attributes = Attribute::get();

        $query = Product::select('products.*');

        if($request->name != ''){
            $query->where('name', 'LIKE', '%'.$request->name.'%')->get();
        }

        if($request->category != ''){
            $productId = CategoryProduct::where('category_id', $request->category)->pluck('product_id')->toArray();
            $query->whereIn('id', $productId);
        }

        if($request->attribute != ''){
            $productId = AttributeProduct::where('attribute_id', $request->attribute)->pluck('product_id')->toArray();
            $query->whereIn('id', $productId);
        }

        if($request->qty){
            $query->where('quantity', 1)->get();
        }

        if($request->export == 'export'){
            return $this->exportBySearchQuery($query);
        }

        if($request->clear == 'clear'){
            return $this->index();
        }

        $products = $query->withTrashed()->orderBy('created_at', 'desc')->paginate(6);

        return view('admin.products.index', compact('products', 'categories', 'suppliers', 'attributes'));
    }

    public function index(){
        $products = Product::withTrashed()->orderBy('created_at', 'desc')->paginate(6);
        $categories = Category::get();
        $suppliers = Supplier::get();
        $attributes = Attribute::get();
        
        // dd($suppliers);
        return view('admin.products.index', compact('products', 'categories', 'suppliers', 'attributes', 'attributeValues'));
    }

    public function store(Request $request){
        // Log::info($request->all());
        // exit();
        $data = $request->all();
        // dd(count($data['attribute_id']));

        // $validator = Validator::make($data, [
        //     'photos' => 'required',
        //     'photos.*' => 'required|image|mimes:jpg,jpeg,png,gif|max:2048',
        //     'category_id' => 'required',
        //     // 'attribute_id' => 'required',
        //     'name' => 'required',
        //     'description' => 'required',
        //     'price' => 'required',
        //     'currency' => 'required'
        // ]);

        // if($validator->fails()) {
        //     return redirect('admin/products')
        //                 ->withErrors($validator)
        //                 ->withInput();
        // }

        DB::beginTransaction();

        try {
            if($data['quantity'] == '' || $data['quantity'] == 0){
                $quantity = 0;
                $stock_status = 0;
            }else{
                $quantity = $data['quantity'];
                $stock_status = 1;
            }
    
            // dd(getType($data['supplier_id']));
            // $supplierId = null;
            // if(array_key_exists('supplier_id', $data) && $data['supplier_id']){
            //     $supplierId = $data['supplier_id'];
            // }
    
            $product = Product::create([
                'supplier_id' => ($data['supplier_id'] != '')? $data['supplier_id']: null,
                'name' => $data['name'],
                'SKU' => $data['sku'],
                'description' => $data['description'],
                // 'stock_status' => $data['stock_status']?? 0,
                'stock_status' => $stock_status,
                'price' => $data['price'],
                'currency' => $data['currency'],
                'quantity' => $quantity,
                'discount_price' => ($data['dprice'] != '')? $data['dprice']: null,
                'discount_from' => ($data['fromdate'] != '')? $data['fromdate']: null,
                'discount_to' => ($data['todate'] != '')? $data['todate']: null,
            ]);
    
            // if(array_key_exists('attribute_id', $data) && $data['attribute_id']){
            if(isset($data['attribute_id']) && isset($data['attribute_value'])){
                sort($data['attribute_id']);
                sort($data['attribute_value']);
                
                $sync_data = [];

                for($i = 0; $i < count($data['attribute_id']); $i++){
                    $sync_data[$i] = ['attribute_id' => $data['attribute_id'][$i], 'values' => $data['attribute_value'][$i]];
                }

                $product->attributes()->attach($sync_data);
            }

            sort($data['category_id']);
            $product->categories()->attach($data['category_id']);
            
            if($request->hasFile('photos')){
                $files = $request->file('photos');
    
                $media_array = [];
                foreach($files as $file){
                    $file_extension = $file->getClientOriginalExtension();
                    $file_name = time().$file->getFilename();
                    Storage::disk('public')->put($file_name.'.'.$file_extension, File::get($file));
                    // $file_path = public_path($file_name);
                    // dd($file_path);
                    $info = pathinfo(Storage::disk('public')->url($file_name));
                    $file_path = $info['dirname'];
                    $file_type = $file->getMimeType();
                    $file_size = $file->getClientSize();
    
                    array_push($media_array, [
                        'product_id' => $product->id,
                        'file_name' => $file_name,
                        'file_path' => $file_path,
                        'file_extension' => $file_extension,
                        'file_type' => $file_type,
                        'file_size' => $file_size
                    ]);
                }
                Media::insert($media_array);
            }

            DB::commit();

            Session::flash('created_message', 'The product has been created');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('error_message', 'Failed !! The product has not been created');
            return redirect()->back();
        }
    }

    public function edit($id){
        $product = Product::find($id);
        
        if($product != null){
            $images = Media::where('product_id', $id)->orderBy('file_size', 'asc')->get();

            $suppliers = Supplier::pluck('name', 'id');

            $categories = Category::pluck('name', 'id');
            $category_product = [];

            foreach($product->categories as $category){
                $category_product[$category->id] = $category;
            }
            // dd($category_product);
            $attributes = Attribute::get();

            $data = [];
            foreach($product->attributes as $attribute){   
                $data[] = [
                    'attribute_id' => $attribute->id,
                    'value' => $attribute->pivot->values,
                    'attributeValue' => AttributeValue::where('attribute_id', $attribute->id)->get()
                ];
            }
            // dd($data);

            return view('admin.products.edit', compact('product', 'images', 'suppliers', 'categories', 'category_product', 'attributes', 'attributeValues', 'attribute_value', 'data'));
            
        }else{
            Session::flash('error_message', 'This product is deleted. So it cannot be edited! ');
            return redirect()->back();
        }
    }

    public function show($id){

    }

    public function update(Request $request, $id){
        // Log::info($request->all());
        // exit();
        $data = $request->all();
        $product = Product::find($id);

        // $validator = Validator::make($data, [
        //     'category_id' => 'required',
        //     // 'attribute_id' => 'required',
        //     'name' => 'required',
        //     'description' => 'required',
        //     'price' => 'required',
        // ]);

        // if($validator->fails()){
        //     return redirect('admin/products')
        //                 ->withErrors($validator)
        //                 ->withInput();
        // }

        if($request->hasFile('photos')){
            $files = $request->file('photos');
            
            $media_array = [];
            foreach($files as $file){
                DB::beginTransaction();

                try {
                // dd($file);
                $file_extension = $file->getClientOriginalExtension();
                $file_name = time().$file->getFilename();

                Storage::disk('public')->put($file_name.'.'.$file_extension, File::get($file));
                $info = pathinfo(Storage::disk('public')->url($file_name));

                $file_path = $info['dirname'];
                $file_type = $file->getMimeType();
                $file_size = $file->getClientSize();

                // array_push($media_array, [
                //     'product_id' => $id,
                //     'file_name' => $file_name,
                //     'file_path' => $file_path,
                //     'file_extension' => $file_extension,
                //     'file_type' => $file_type,
                //     'file_size' => $file_size
                // ]);

                $media = Media::create([
                    'product_id' => $id,
                    'file_name' => $file_name,
                    'file_path' => $file_path,
                    'file_extension' => $file_extension,
                    'file_type' => $file_type,
                    'file_size' => $file_size
                ]); 
                DB::commit();
                // dd($data['photos']);

                if(in_array($file, $data['photos'])){
                    $key = array_search($file, $data['photos']);
                    $arr_set = array_set($data['photos'], $key, "$media->id");
                }

                } catch (\Exception $e) {
                    DB::rollBack();

                    Session::flash('error_message', 'Failed !! The product image has not been created');
                    return redirect()->back();
                }
            }
            // $media = Media::insert($media_array);
        }
        // dd($data['photos']);
        $edited_photos = Media::where('product_id', $id)->whereNotIn('id', $data['photos'])->delete();

        if($data['quantity'] == '' || $data['quantity'] == 0){
            $quantity = 0;
            $stock_status = 0;
        }else{
            $quantity = $data['quantity'];
            $stock_status = 1;
        }

        $product->update([
            'supplier_id' => ($data['supplier_id'] != '')? $data['supplier_id']: null,
            'name' => $data['name'],
            'SKU' => $data['sku'],
            'description' => $data['description'],
            'stock_status' => $stock_status,
            'price' => $data['price'],
            'currency' => $data['currency'],
            'quantity' => $quantity,
            'discount_price' => ($data['dprice'] != '')? $data['dprice']: null,
            'discount_from' => ($data['fromdate'] != '')? $data['fromdate']: null,
            'discount_to' => ($data['todate'] != '')? $data['todate']: null,
        ]);

        sort($data['category_id']);
        $product->categories()->sync($data['category_id']);

        if(isset($data['attribute_id']) && isset($data['attribute_value'])){
            $product->attributes()->detach();
            sort($data['attribute_id']);
            sort($data['attribute_value']);
            
            $sync_data = [];

            for($i = 0; $i < count($data['attribute_id']); $i++){
                $sync_data[$i] = ['attribute_id' => $data['attribute_id'][$i], 'values' => $data['attribute_value'][$i]];
            }
            
            $product->attributes()->attach($sync_data);
            
        }

        Session::flash('updated_message', 'The product has been updated');

        return redirect('admin/products');
    }

    public function destroy($id){
        $product = Product::findOrFail($id);
        // $product->categories()->detach();
        // $product->attributes()->detach();
        $product->delete();

        Session::flash('deleted_message', 'The product has been deleted');

        return redirect()->back();
    }

    public function restore($id) 
    {
        $product = Product::withTrashed()->find($id)->restore();
        return redirect()->back();
    }

    public function getAjaxValue(Request $request){
        // dd($request);
        $values = AttributeValue::where('attribute_id', $request->id)->get();
        // dd($values);

        return response()->json($values);
    }

    public function autocomplete(Request $request){
        $data = Product::where('name', 'LIKE', '%'.$request->name.'%')->get();

        return response()->json($data);
    }

    public function exportBySearchQuery($query){
        $products = $query->orderBy('created_at', 'desc')->get();
        
        Excel::create('Product Lists', function($excel) use($products) {
            $excel->sheet('Product Lists', function($sheet) use($products) {
                 $sheet->loadView('admin.products.excel_export', compact('products'));
            });
        })->export('xlsx');
    }
}
