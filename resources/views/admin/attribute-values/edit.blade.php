@extends('layouts.admin')

@section('content')

	<h1>Attribute Value</h1>

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

    <ul class="nav nav-tabs">
		<li role="presenstation" class="active"><a href="#view"><strong>Attribute Value Lists - Edit</strong></a></li>
	</ul>

    <div class="container " style="padding: 10px;">
        <div class="row clearfix">
            <div class="col-md-12">
                <a href="{{ route('admin.attributes.index') }}" class="btn btn-warning" style="float:right; margin-bottom:20px;">Back to Attribute</a>   
            </div>
        </div>
        
        <div class="row clearfix">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Attribute Value Lists
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped text-center">
                            <thead">
                                <th class="text-center">ID</th>
                                <th class="text-center">Values</th>
                                <th class="text-center">Actions</th>
                            </thead>

                            <tbody>
                            <?php $i=0; ?>
                                @foreach($attribute->values as $val)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $val->name }}</td>
                                    <td>
                                        <a class="btn btn-primary btn-xs" disabled><i class="material-icons">edit</i></a>
                                        <form action="" method="post">
                                        @csrf
                                        @method('DELETE')
                                            <button class="btn btn-danger btn-xs" disabled><i class="material-icons">delete</i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit Attribute Value
                    </div>

                    <div class="panel-body">
                        <form action="{{ route('admin.attribute-values.update', $value->id) }}" method="POST" class="form-horizontal">
                        @csrf
                        @method('PATCH')

                            <div class="row clearfix">
                                <div class="form-group col-md-12">
                                    <label for="attribute_id" class="control-label">Attribute Type</label>
                                    <select id="attribute_id" name="attribute_id" class="form-control">
                                        <option value="{{$attribute->id}}" selected>{{ $attribute->name }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="form-group col-md-12">
                                    <label for="name" class="control-label">Attribute Value</label><br>
                                    <input type="text"  class="form-control" name="name" id="name" value="{{ $value->name }}"/>
                                </div>
                            </div>

                            <div class="row clearfix" style="float:right;">
                                <button type="submit" class="btn btn-success">
                                    Update
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection