<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use App\User;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::paginate(10);

        return view('admin.suppliers.index', compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return view('admin.supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|unique:suppliers,name',
            'address1' => 'required',
            // 'phone' => 'required|regex:/(09)[0-9]{9}/',
            'phone' => 'required',
            'contact' => 'required',
            'email' => 'required|unique:suppliers,mail',
        ]);

        if($validator->fails()) {
            return redirect('admin/suppliers')
                        ->withErrors($validator)
                        ->withInput();
        }

        DB::beginTransaction();

        try {
            $supplier = new Supplier();
            $supplier->user_id = Auth::user()->id;
            $supplier->name = $data['name'];
            $supplier->address1 = $data['address1'];
            $supplier->address2 = ($data['address2'] != '') ? $data['address2'] : null;
            $supplier->phone = $data['phone'];
            $supplier->contact_person = $data['contact'];
            $supplier->mail = $data['email'];
            $supplier->description = ($data['description'] != '') ? $data['description'] : null;
            $supplier->save();

            DB::commit();

            Session::flash('created_message', 'The supplier has been created');
            return redirect()->back();
        } catch (\Exception $e) {

            DB::rollBack();

            Session::flash('error_message', 'Failed !! The supplier has not been created');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = Supplier::find($id);
// dd($supplier);
        return view('admin.suppliers.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|unique:suppliers,name,'.$id,
            'address1' => 'required',
            // 'phone' => 'required|regex:/(09)[0-9]{9}/',
            'phone' => 'required|unique:suppliers,phone,'.$id,
            'contact' => 'required',
            'email' => 'required|unique:suppliers,mail,'.$id,
        ]);

        if($validator->fails()) {
            return redirect('admin/suppliers')
                        ->withErrors($validator)
                        ->withInput();
        }

        $supplier = Supplier::find($id);
        $supplier->user_id = Auth::user()->id;
        $supplier->name = $data['name'];
        $supplier->address1 = $data['address1'];
        $supplier->address2 = ($data['address2'] != '' ) ? $data['address2'] : null;
        $supplier->phone = $data['phone'];
        $supplier->contact_person = $data['contact'];
        $supplier->mail = $data['email'];
        $supplier->description = ($data['description'] != '' ) ? $data['description'] : null;
        $supplier->update();

        Session::flash('updated_message', 'The supplier has been updated');
        return redirect('admin/suppliers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::find($id);
        $supplier->delete();

        Session::flash('deleted_message', 'The supplier has been deleted');
        return redirect('admin/suppliers');
    }
}
