@extends('layouts.admin')

@section('content')

{{ Breadcrumbs::render('attributes', $attributes) }}

	<h1>Attribute</h1>

    @include('includes.message')

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

    <ul class="nav nav-tabs">
		<li role="presenstation" class="active"><a href="#view"><strong>Attribute Lists - Create</strong></a></li>
	</ul>

    <div class="container " style="padding: 10px;">
        
        <div class="row-clearfix">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Attribute Lists
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped text-center">
                            <thead">
                                <th class="text-center">ID</th>
                                <th class="text-center">Types</th>
                                <th class="text-center">Values</th>
                                <th class="text-center">Actions</th>
                            </thead>

                            <tbody>
                                @foreach($attributes as $attribute)
                                <tr>
                                    <td>{{ $attribute->id }}</td>
                                    <td>{{ $attribute->name }}</td>
                                    <td>
                                    <a href="{{ route('admin.attribute-values', $attribute->id) }}">
                                        {{ $attribute->values->count() }}
                                    </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.attributes.edit', $attribute->id) }}" class="btn btn-primary btn-xs"><i class="material-icons">edit</i></a>

                                        @if($attribute->trashed())
                                            <form action="{{ route('admin.attributes.restore', $attribute->id) }}" method="get">
                                            @csrf
                                                <button class="btn btn-warning btn-xs"><i class="material-icons">restore</i></button>
                                            </form>
                                        @else
                                            <form action="{{ route('admin.attributes.destroy', $attribute->id) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                                <button class="btn btn-danger btn-xs"><i class="material-icons">delete</i></button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $attributes->links() }}
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        New Attribute
                    </div>

                    <div class="panel-body">
                        <form action="{{ route('admin.attributes.store') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                            <div class="row clearfix">
                                <div class="form-group col-md-12">
                                    <label for="name" class="control-label">Attribute</label>
                                    <input type="text" name="name" id="name" class="form-control">
                                </div>
                            </div>

                            <div class="row clearfix" style="float:right;">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-plus"></i> Add
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
