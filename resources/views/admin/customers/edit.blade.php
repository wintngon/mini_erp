@extends('layouts.admin')

@section('content')

	<h1>Customers</h1>

	@include('includes.message')

	@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

	<!-- Start .nav nav-tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presenstation" class="active"><a href="#"><strong>Edit Customer</strong></a></li>
	</ul>
    <!-- End .nav nav-tabs -->

    <div class="tab-content">

		<!-- Edit Product -->
        <div style="padding: 20px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Customer
                </div>

                <div class="panel-body">
                    <form action="{{ route('admin.customers.update', $customer->id) }}" method="POST" class="form-horizontal">
                    @csrf
                    @method('PATCH')

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="name" class="control-label">Name</label>
                                <input type="text" name="name" id="name" class="form-control" value="{{ $customer->name }}">
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="phone" class="control-label">Phone</label>
                                <input type="text" name="phone" id="phone" class="form-control" value="{{ $customer->phone }}">
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="address1" class="control-label">Address 1</label>
                                <textarea name="address1" id="address1" cols="30" rows="10" class="form-control">{{ $customer->address1 }}</textarea>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="address2" class="control-label">Address 2</label>
                                <textarea name="address2" id="address2" cols="30" rows="10" class="form-control">{{ $customer->address2 }}</textarea>
                            </div>
                        </div>

                        <div class="row clearfix" style="float:right;">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>
                    </form>
                </div>
            </div>
		</div>			
	</div>
    
@stop