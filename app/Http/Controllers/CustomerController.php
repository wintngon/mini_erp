<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::withTrashed()->paginate(10);
        return view('admin.customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required',
            'address1' => 'required',
            'phone' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('admin/customers')
                        ->withErrors($validator)
                        ->withInput();
        }

        DB::beginTransaction();

        try {
            $customer = new Customer();
            $customer->name = $data['name'];
            $customer->phone = $data['phone'];
            $customer->address1 = $data['address1'];
            $customer->address2 = ($data['address2'] != '')? $data['address2']: null;
            $customer->save();
            DB::commit();

            Session::flash('created_message', 'The customer has been created');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            Session::flash('error_message', 'Failed !! The customer has not been created');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);

        return view('admin.customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required',
            'address1' => 'required',
            'phone' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $customer = Customer::find($id);
        $customer->name = $data['name'];
        $customer->phone = $data['phone'];
        $customer->address1 = $data['address1'];
        $customer->address2 = ($data['address2'] != '')? $data['address2']: null;
        $customer->update();

        Session::flash('updated_message', 'The customer has been updated');
        return redirect('admin/customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();

        Session::flash('deleted_message', 'The customer has been deleted');
        return redirect('admin/customers');
    }

    public function restore($id){
        $customer = Customer::withTrashed()->find($id)->restore();
        return redirect()->back();
    }
}
