@extends('layouts.admin')

@section('content')

{{ Breadcrumbs::render('orders', $orders) }}
	<h1>Orders</h1>

	@include('includes.message')

	@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

	<!-- Start .nav nav-tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presenstation" class="active"><a href="#view" aria-controls="view" role="tab" data-toggle="tab"><strong>View Orders</strong></a></li>
		<li role="presenstation"><a href="#create" aria-controls="create" role="tab" data-toggle="tab"><strong>Create Order</strong></a></li>
	</ul>
    <!-- End .nav nav-tabs -->

    <div class="tab-content">
		<!-- Show Orders -->
		<div role="tabpanel1" class="tab-pane fade in active" id="view" style="padding: 20px;">
            <div class="row">	
				<form action="{{ route('admin.orders.search') }}">
					<div class="col-md-12">
						<div class="form-group col-md-6">
							<label>Customer Name</label>
							<input type="text" name="name" id="keyword" class="name form-control" value="{{ old('name', Input::get('name') != null ? Input::get('name') : '' ) }}">
							<div id="customer_list" style="position:relative;"></div>
						</div>

                        <div class="form-group col-md-6">
							<label>Order No</label>
							<input type="text" name="order_no" id="order_keyword" class="order_no form-control" value="{{ old('order_no', Input::get('order_no') != null ? Input::get('order_no') : '' ) }}">
							<div id="order_list" style="position:relative;"></div>
						</div>

						<div class="form-group col-md-4">
							<label>Channel</label>
							<select name="channel" id="channel" class="form-control"> 
							<option value="">Select Channel</option>
							@foreach(config('constant.channels') as $key => $channel)
								<option value="{{ $key }}" {{ Input::get('channel') == $key ? 'selected' : '' }}>{{ $channel }}</option>
							@endforeach
							</select>
						</div>

						<div class="form-group col-md-4">
							<label>Status</label>
							<select name="status" id="status" class="form-control"> 
							<option value="">Select Status</option>
							@foreach(config('constant.status') as $key => $status)
								<option value="{{ $key }}" {{ Input::get('status') == $key ? 'selected' : '' }}>{{ $status }}</option>
							@endforeach
							</select>
						</div>

						<div class="form-group col-md-4">
							<label>Payment Method</label>
							<select name="payment_method" id="payment_method" class="form-control"> 
							<option value="">Select Payment Method</option>
							@foreach(config('constant.payment-method') as $key => $payment_method)
								<option value="{{ $key }}" {{ Input::get('payment_method') == $key ? 'selected' : '' }}>{{ $payment_method }}</option>
							@endforeach
							</select>
						</div>
					</div>
                    <button type="submit" name="clear" value="clear" class="clear btn btn-default"  style="float:right; margin-top:20px;">Clear</button>
					<button type="submit" name="export"  value="export" class="btn btn-info" style="float:right; margin-top:20px;">Export</button>
					<input type="submit" value="Search" class="btn btn-success" style="float:right; margin-top:20px;">
					
				</form>	
			</div>

            <hr>
			<div class="row">
				<div class="table-responsive col-md-12">
				<table class="table table-bordered ">
					<thead>
					<tr>
                        <th>Id</th>
                        <th>Order No</th>
						<th>Channel</th>
                        <th>Status</th>
                        <th>Customer Name</th>
                        <th>Payment Method</th>
                        <th>Actions</th>
					</tr>
					</thead>
					<tbody>	
                    @foreach($orders as $order)
                    <tr>
                        <td>{{$order->id}}</td>
                        <td>{{$order->order_no}}</td>
                        <td>{{$order->channel}}</td>
                        <td>{{$order->status}}</td>
                        <td>
                            {{ $order->customer->name }}
                        </td>
                        <td>{{$order->payment_method}}</td>
                        <td>
                        <a href="{{ route('admin.orders.edit', $order->id) }}" class="btn btn-primary btn-xs"><i class="material-icons">edit</i></a>

                        @if($order->trashed())
                            <form action="{{ route('admin.orders.restore', $order->id) }}" method="get">
                            @csrf
                                <button class="btn btn-warning btn-xs"><i class="material-icons">restore</i></button>
                            </form>
                        @else
                            <form action="{{ route('admin.orders.destroy', $order->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                                <button class="btn btn-danger btn-xs"><i class="material-icons">delete</i></button>
                            </form>
                        @endif
                        </td>
                    </tr>
                    @endforeach
					</tbody>
				</table>
				
				</div>
			</div>
		  	<!-- End .table-responsive -->

			<!-- Pagination -->
		  	<div class="row" style="padding-left: 10px;">
		  		{{$orders->links()}}
		  	</div>
		</div>
		<!-- End .tab-pane -->
		<!-- End Show Product -->

		<!-- Create Product -->
        <div role="tabpanel2" class="tab-pane fade" id="create" style="padding: 20px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Order
                </div>

                <div class="panel-body">
                    <form action="{{ route('admin.orders.store') }}" method="POST" class="form-horizontal" id="form_validation">
                    {{ csrf_field() }}

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="order_no" class="control-label">Order No</label>
                                <input type="text" name="order_no" id="order_no" class="form-control" required>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="channel" class="control-label">Channel</label>
                                <select name="channel" id="channel" class="form-control" required>
                                    <option value="" disabled selected>Select Channel</option>
                                    @foreach(config('constant.channels') as $key=>$channel)
                                    <option value="{{ $key }}" >{{ $channel }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="status" class="control-label">Status</label>
                                <select name="status" id="status" class="form-control" required>
                                    <option value="" disabled selected>Select Status</option>
                                    @foreach(config('constant.status') as $key => $status)
                                    <option value="{{ $key }}">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                            <label for="customer" class="control-label">Customer</label>
                                <select name="customer_id" id="customer" class="form-control" required>
                                    <option value="" disabled selected>Select Customer</option>
                                    @foreach($customers as $customer)
                                    <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                            <label for="payment_method" class="control-label">Payment Method</label>
                                <select name="payment_method" id="payment_method" class="form-control" required>
                                    <option value="" disabled selected>Select Payment-method</option>
                                    @foreach(config('constant.payment-method') as $key => $payment)
                                    <option value="{{ $key }}">{{ $payment }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label class="control-label">Order Product</label>
                                <table class="table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center">Product SKU</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">Regular Price</th>
                                            <th class="text-center">Discount Price</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center"><button type="button" name="add" id="addButton" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr data-id="1" id="row1">
                                            <td><img src="http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg" class="product_img" style="max-height: 75px; border: 2px solid; padding: 1px; cursor: pointer; width: 100px; height: 100px;background-size: cover; background-repeat:no-repeat; display: inline-block;"></td>
                                            <td><input name="product[1]" type="text" class="autocomplete_txt" style="width: 100%;" required><div id="product_list" style="position: relative;"></div></td>
                                            <td><input name="qty[1]" type="number" min='1' class="qty" ></td>
                                            <td><input name="regular_price[1]" type="text" class="regular_price" readonly ></td>
                                            <td><input name="discount_price[1]" type="text" class="discount_price" readonly></td>
                                            <td><input name="amount[1]" type="text" class="amount" readonly ></td>
                                            <td><button type="button" id="1" name="btn_remove" class="btn btn-danger btn_remove"><span class="glyphicon glyphicon-minus"></span></button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label>Total Amount - </label>
                                <input type="text" name="total_amount" class="total_amount" value="{{ old('total_amount') }}" readonly>
                            </div>
                        </div>

                        <div class="row clearfix" style="float:right;">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-plus"></i> Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
		</div>			
	</div>
@stop

@section('scripts')
<script>
    $(document).ready(function(){
        // form validation jquery
		$('#form_validation').validate();
        
        // dynamic add/remove row 
        var i = 1;
        $('#addButton').on('click', function(){
            i++;
            $('tbody').append('<tr data-id="'+i+'" id="row'+i+'">\
                                <td><img src="http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg" class="product_img" style="max-height: 75px; border: 2px solid; padding: 1px; cursor: pointer; width: 100px; height: 100px;background-size: cover; background-repeat:no-repeat; display: inline-block;"></td>\
                                <td><input name="product['+i+']" type="text" class="autocomplete_txt" style="width: 100%;" required><div id="product_list" style="position: relative"></div></td>\
                                <td><input name="qty['+i+']" type="number" min="1" class="qty" ></td>\
                                <td><input name="regular_price['+i+']" type="text" class="regular_price" readonly></td>\
                                <td><input name="discount_price['+i+']" type="text" class="discount_price" readonly></td>\
                                <td><input name="amount['+i+']" type="text" class="amount" readonly></td>\
                                <td><button type="button" id="'+i+'" name="btn_remove" class="btn btn-danger btn_remove"><span class="glyphicon glyphicon-minus"></span></button></td>\
                                </tr>');
        });

        $(document).on('click', '.btn_remove', function(){
            var tr = $(this).closest('tr');
            tr.remove();
            var amount = tr.find('.amount').val();  //console.log(amount);
            var sub_amount = $('.total_amount').val(); //console.log(sub_amount);
            var total_amount = sub_amount - amount; //console.log(total_amount);
            $('.total_amount').val(total_amount);
        });

        $(document).on('keyup', '.autocomplete_txt', function(){
            var id = $(this).closest('tr').data("id");  //console.log(id);
            var tr = $(this).closest('tr');
            var query = $(this).val();
            //console.log(query);
            var op = '';
            
            if(query.length > 0){
                $.ajax({
                    url: "{{ route('admin.orders.autocomplete_name') }}",
                    type: 'get',
                    data: {'product': query},
                    success: function(data){
                        //console.log(data);
                        op += '<ul class="list-group" style="position: absolute; width: 100%; max-height: 120px; overflow: auto; z-index: 1">';
                        for(var p=0; p<data.length; p++){
                            op += '<li class="product_name_list list-group-item">'+data[p].name+'</li>';
                        }
                        op += '</ul>';
                        tr.find('#product_list').html(op);
                    }
                });
            }else{
				tr.find('#product_list').html('');
			}     	    		
        });
        
        $(document).on('click', '.product_name_list', function(){
            var id = $(this).closest('tr').data("id");  //console.log(id);
            var tr = $(this).closest('tr');
            var product = $(this).text();

            tr.find('.autocomplete_txt').val(product);
            tr.find('#product_list').html('');

            // var amount = tr.find('.amount').val();  console.log(amount);
            // var sub_amount = $('.total_amount').val(); console.log(sub_amount);
            // var total_amount = sub_amount - amount; console.log(total_amount);
            // $('.total_amount').val(total_amount);

            if(product){
                // console.log(product);
                $.ajax({
                    url: "{{ route('admin.orders.autocomplete_multiple') }}",
                    type: 'get',
                    data: {'name': product},
                    dataType: "json",
                    success: function(data){
                        // console.log(data.media);
                        var img = window.location.origin+'/uploads/'+data.media.file_name+'.'+data.media.file_extension;    //console.log(img);
                        // tr.find(".product_img").css("background-image", "url("+img+")");
                        tr.find(".product_img").attr("src", img);
                        tr.find(".regular_price").val(data.val[0].price);
                        tr.find(".discount_price").val(data.val[0].discount_price)
                        tr.find(".qty").val(1);

                        var regular_price = tr.find('.regular_price').val();  //console.log(regular_price);
                        var discount_price = tr.find('.discount_price').val(); //console.log(discount_price);
                        var amount;  
                        if(discount_price){
                            amount = discount_price;
                        }
                        else{
                            amount = regular_price;
                        }
                        tr.find('.amount').val(amount);
                        var total_amount = 0;
                        $(".amount").each(function(){
                            amount = $(this).val(); console.log(amount);
                            total_amount += parseFloat(amount) || 0;
                        });
                        $('.total_amount').val(total_amount);
                    }
                }); 
            }
        });

        $(document).on('input', '.qty', function(){
            var id = $(this).closest('tr').data("id");console.log(id);
            var tr = $(this).closest('tr');
            var qty = tr.find(".qty").val();  // console.log(qty);
            var regular_price = tr.find('.regular_price').val();  //console.log(regular_price);
            var discount_price = tr.find('.discount_price').val(); //console.log(discount_price);
            var total_amount;
            if(discount_price){
                amount = discount_price * qty;
            }
            else{
                amount = regular_price * qty;
            }
            tr.find('.amount').val(amount);

            var total_amount = 0;
            $(".amount").each(function(){
                var amount = $(this).val(); //console.log(amount);
                total_amount += parseFloat(amount) || 0;
            });
            $('.total_amount').val(total_amount);
        })

        // customer-name autocomplete search
        $(document).on('keyup', '.name', function(){
            var name = $(this).val();    //console.log(name);
            var op = '';
            if(name.length > 0){
                $.ajax({
                    url: "{{ route('admin.orders.getCustomerName') }}",
                    type: 'get',
                    data: {'name': name},
                    success: function(data){
                        console.log(data);
                        op += '<ul class="list-group" style="position: absolute; width: 100%; max-height: 120px; overflow: auto; z-index: 1">';
                        for(var n=0; n<data.length; n++){
                            op += '<li class="name_list list-group-item">'+data[n].name+'</li>';
                        }
                        op += '</ul>';
                        $('#customer_list').html(op);
                    }
                });
            }else{
				$('#customer_list').html('');
			} 
        });

        // order_no autocomplete search
        $(document).on('click', '.name_list', function(){
			var name = $(this).text();
			$('#keyword').val(name);
			$('#customer_list').html('');	
		});

        $(document).on('keyup', '.order_no', function(){
            var order_no = $(this).val(); console.log(order_no);
            var op = '';
            if(order_no.length > 0){
                $.ajax({
                    url: "{{ route('admin.orders.getOrderNo') }}",
                    type: 'get',
                    data: {'order_no': order_no},
                    success: function(data){
                        console.log(data);
                        op += '<ul class="list-group" style="position: absolute; width: 100%; max-height: 120px; overflow: auto; z-index: 1">';
                        for(var o=0; o<data.length; o++){
                            op += '<li class="number_list list-group-item">'+data[o].order_no+'</li>';
                        }
                        op += '</ul>';
                        $('#order_list').html(op);
                    }
                });
            }else{
                $('#order_list').html('');
            }
        });

        $(document).on('click', '.number_list', function(){
			var order_no = $(this).text();
			$('#order_keyword').val(order_no);
			$('#order_list').html('');	
		});

        //clear search
        $(document).on('click', '.clear', function(){
            $('#keyword').val('');
            $('#order_keyword').val('');
            $('#channel').val('');
            $('#status').val('');
            $('#payment_method').val('');
        });
    });
</script>
@stop

@section('styles')
<style>
    label.error{
        color: #f56042;
        font-size: 12px;
        display: block;
    }
</style>

@stop
