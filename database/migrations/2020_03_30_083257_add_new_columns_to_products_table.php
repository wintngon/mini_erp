<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('SKU')->after('name')->nullable();
            $table->integer('supplier_id')->after('id')->nullable();
            $table->string('currency')->after('price')->nullable();
            $table->double('discount_price', 8, 2)->after('quantity')->nullable();
            $table->dateTime('discount_from')->after('discount_price')->nullable();
            $table->dateTime('discount_to')->after('discount_from')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['SKU', 'supplier_id', 'currency', 'discount_price', 'discount_from', 'discount_to']);
        });
    }
}
