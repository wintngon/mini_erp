<?php

namespace App\Http\Controllers;

use DB;
use Log;
use Validator;
use App\Attribute;
use App\AttributeValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Attributecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attributes = Attribute::withTrashed()->paginate(10);
        return view('admin.attributes.index', compact('attributes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|unique:attributes',
        ]);

        if ($validator->fails()) {
            return redirect('admin/attributes')
                        ->withErrors($validator)
                        ->withInput();
        }

        DB::beginTransaction();

        try {
            $attribute = new Attribute();
            $attribute->name = $data['name'];
            $attribute->save();

            DB::commit();

            Session::flash('created_message', 'The attribute has been created');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            Session::flash('error_message', 'Failed !! The attribute has not been created');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $attribute = Attribute::find($id);
        $attribute = Attribute::with('values')->where('id', $id)->first();
        // Log::info($attribute);

        $attributes = Attribute::get();

        if($attribute != null){
            return view('admin.attributes.edit', compact('attribute', 'attributes'));
        }else{
            Session::flash('error_message', 'This attribute is deleted. So it cannot be edited! ');
            return redirect()->back();
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|unique:attributes,name,'.$id
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $attribute = Attribute::find($id);
        $attribute->name = $data['name'];
        $attribute->update();

        Session::flash('updated_message', 'The attribute has been updated');
        return redirect('admin/attributes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $attribute = Attribute::find($id);
        $attribute = Attribute::with('values')->where('id', $id)->first();
        // dd($attribute->values());
        $attribute->values()->delete();
        $attribute->delete();

        Session::flash('deleted_message', 'The attribute has been deleted');
        return redirect('admin/attributes');
    }

    public function restore($id) 
    {
        $attribute = Attribute::withTrashed()->find($id)->restore();
        return redirect()->back();
    }
}
