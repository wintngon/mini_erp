@extends('layouts.admin')

@section('content')

	<h1>Orders</h1>

	@include('includes.message')

	@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <ul class="nav nav-tabs" role="tablist">
    <li role="presenstation" class="active"><a href="#edit"><strong>Edit Order</strong></a></li>
    </ul>
    
    <div class="tab-content">
        <div id="edit" style="padding: 20px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Order
                </div>

                <div class="panel-body">
                    <form action="{{ route('admin.orders.update', $order->id) }}" method="POST" class="form-horizontal" id="form_validation">
                    @csrf
                    @method('PATCH')

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="order_no" class="control-label">Order No</label>
                                <input type="text" name="order_no" id="order_no" class="form-control" value="{{ $order->order_no }}" required>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="channel" class="control-label">Channel</label>
                                <select name="channel" id="channel" class="form-control" required>
                                    <option value="" disabled selected>Select Channel</option>
                                    @foreach(config('constant.channels') as $key=>$channel)
                                        <option value="{{ $key }}" {{ $key == $order->channel ? 'selected' : '' }}>{{ $channel }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label for="status" class="control-label">Status</label>
                                <select name="status" id="status" class="form-control" required>
                                    <option value="" disabled selected>Select Status</option>
                                    @foreach(config('constant.status') as $key => $status)
                                    <option value="{{ $key }}" {{ $key == $order->status ? 'selected' : '' }} >{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                            <label for="customer" class="control-label">Customer</label>
                                <select name="customer_id" id="customer" class="form-control" required>
                                    <option value="" disabled selected>Select Customer</option>
                                    @foreach($customers as $customer)
                                    <option value="{{ $customer->id }}" {{ $customer->id == $order->customer_id ? 'selected' : '' }}>{{ $customer->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                            <label for="payment_method" class="control-label">Payment Method</label>
                                <select name="payment_method" id="payment_method" class="form-control" required>
                                    <option value="" disabled selected>Select Payment-method</option>
                                    @foreach(config('constant.payment-method') as $key => $payment)
                                    <option value="{{ $key }}" {{ $key == $order->payment_method ? 'selected' : '' }}>{{ $payment }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="form-group col-md-12">
                                <label class="control-label">Order Product</label>
                                <table class="table table-bordered text-center" >
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center">Product SKU</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">Regular Price</th>
                                            <th class="text-center">Discount Price</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center"><button type="button" name="add" id="addButton" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($pivotData as $key => $value)
                                        <tr data-id="{{$key}}" id="row{{$key}}">
                                            <td><img src="{{ url('uploads/'.$value['media']->file_name.'.'.$value['media']->file_extension) }}" class="product_img" style="max-height: 75px; border: 2px solid; padding: 1px; cursor: pointer; width: 100px; height: 100px;background-size: cover; background-repeat:no-repeat; display: inline-block;"></td>
                                            <td><input name="product[{{$key}}]" type="text" class="autocomplete_txt" style="width: 100%;" value="{{ $value['product'] }}" required><div id="product_list" style="position: relative;"></div></td>
                                            <td><input name="qty[{{$key}}]" type="number" min='1' class="qty" value="{{ $value['quantity'] }}"></td>
                                            <td><input name="regular_price[{{$key}}]" type="text" class="regular_price" value="{{ $value['regular_price'] }}"  readonly></td>
                                            <td><input name="discount_price[{{$key}}]" type="text" class="discount_price" value="{{ $value['discount_price'] }}" readonly></td>
                                            <td><input name="amount[{{$key}}]" type="text" class="amount" value="{{ $value['amount'] }}" readonly></td>
                                            <td><button type="button" id="1" name="btn_remove" class="btn btn-danger btn_remove"><span class="glyphicon glyphicon-minus"></span></button></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label>Total Amount - </label>
                                <input type="text" name="total_amount" class="total_amount" value="{{ $order->total_amount }}" readonly>
                            </div>
                        </div>

                        <div class="row clearfix" style="float:right;">
                            <button type="submit" class="btn btn-success">
                                UPDATE
                            </button>
                        </div>
                    </form>
                </div>
            </div>
		</div>
    </div>
@stop
@section('scripts')
<script>
    $(document).ready(function(){
        // form validation jquery
		$('#form_validation').validate();
                
        // dynamic add/remove row 
        var i = 1;
        $('#addButton').on('click', function(){
            i++;
            $('tbody').append('<tr data-id="'+i+'" id="row'+i+'">\
                                <td><img src="http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg" class="product_img" style="max-height: 75px; border: 2px solid; padding: 1px; cursor: pointer; width: 100px; height: 100px;background-size: cover; background-repeat:no-repeat; display: inline-block;"></td>\
                                <td><input name="product['+i+']" type="text" class="autocomplete_txt" style="width: 100%;" required><div id="product_list" style="position: relative"></div></td>\
                                <td><input name="qty['+i+']" type="number" min="1" class="qty" ></td>\
                                <td><input name="regular_price['+i+']" type="text" class="regular_price" readonly></td>\
                                <td><input name="discount_price['+i+']" type="text" class="discount_price" readonly></td>\
                                <td><input name="amount['+i+']" type="text" class="amount" readonly></td>\
                                <td><button type="button" id="'+i+'" name="btn_remove" class="btn btn-danger btn_remove"><span class="glyphicon glyphicon-minus"></span></button></td>\
                                </tr>');
        });

        $(document).on('click', '.btn_remove', function(){
            var tr = $(this).closest('tr');
            tr.remove();
            var amount = tr.find('.amount').val();  //console.log(amount);
            var sub_amount = $('.total_amount').val(); //console.log(sub_amount);
            var total_amount = sub_amount - amount; //console.log(total_amount);
            $('.total_amount').val(total_amount);
        });

        $(document).on('keyup', '.autocomplete_txt', function(){
            var id = $(this).closest('tr').data("id");  //console.log(id);
            var tr = $(this).closest('tr');
            var query = $(this).val();
            //console.log(query);
            var op = '';
            
            if(query.length > 0){
                $.ajax({
                    url: "{{ route('admin.orders.autocomplete_name') }}",
                    type: 'get',
                    data: {'product': query},
                    success: function(data){
                        //console.log(data);
                        op += '<ul class="list-group" style="position: absolute; width: 100%; max-height: 120px; overflow: auto; z-index: 1">';
                        for(var p=0; p<data.length; p++){
                            op += '<li class="list-group-item">'+data[p].name+'</li>';
                        }
                        op += '</ul>';
                        tr.find('#product_list').html(op);
                    }
                });
            }else{
				tr.find('#product_list').html('');
			}     	    		
        });
        
        $(document).on('click', '.list-group-item', function(){
            var id = $(this).closest('tr').data("id");  //console.log(id);
            var tr = $(this).closest('tr');
            var product = $(this).text();

            tr.find('.autocomplete_txt').val(product);
            tr.find('#product_list').html('');

            // var amount = tr.find('.amount').val();  console.log(amount);
            // var sub_amount = $('.total_amount').val(); console.log(sub_amount);
            // var total_amount = sub_amount - amount; console.log(total_amount);
            // $('.total_amount').val(total_amount);

            if(product){
                // console.log(product);
                $.ajax({
                    url: "{{ route('admin.orders.autocomplete_multiple') }}",
                    type: 'get',
                    data: {'name': product},
                    dataType: "json",
                    success: function(data){
                        // console.log(data.media);
                        var img = window.location.origin+'/uploads/'+data.media.file_name+'.'+data.media.file_extension;    //console.log(img);
                        // tr.find(".product_img").css("background-image", "url("+img+")");
                        tr.find(".product_img").attr("src", img);
                        tr.find(".regular_price").val(data.val[0].price);
                        tr.find(".discount_price").val(data.val[0].discount_price)
                        tr.find(".qty").val(1);

                        var regular_price = tr.find('.regular_price').val();  //console.log(regular_price);
                        var discount_price = tr.find('.discount_price').val(); //console.log(discount_price);
                        var amount;  
                        if(discount_price){
                            amount = discount_price;
                        }
                        else{
                            amount = regular_price;
                        }
                        tr.find('.amount').val(amount);
                        var total_amount = 0;
                        $(".amount").each(function(){
                            amount = $(this).val(); console.log(amount);
                            total_amount += parseFloat(amount) || 0;
                        });
                        $('.total_amount').val(total_amount);
                    }
                }); 
            }
        });

        $(document).on('input', '.qty', function(){
            var id = $(this).closest('tr').data("id");console.log(id);
            var tr = $(this).closest('tr');
            var qty = tr.find(".qty").val();  // console.log(qty);
            var regular_price = tr.find('.regular_price').val();  //console.log(regular_price);
            var discount_price = tr.find('.discount_price').val(); //console.log(discount_price);
            var total_amount;
            if(discount_price){
                amount = discount_price * qty;
            }
            else{
                amount = regular_price * qty;
            }
            tr.find('.amount').val(amount);

            var total_amount = 0;
            $(".amount").each(function(){
                var amount = $(this).val(); //console.log(amount);
                total_amount += parseFloat(amount) || 0;
            });
            $('.total_amount').val(total_amount);
        })
    });
</script>
@stop

@section('styles')
<style>
    label.error{
        color: #f56042;
        font-size: 12px;
        display: block;
    }
</style>

@stop