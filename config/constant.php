<?php 
    return
    [
        'channels' => [
            'facebook' => 'Facebook',
            'website' => 'Website',
            'phone' => 'Phone'
        ],
        
        'currency' => [
            'mmk' => 'MMK',
            'usd' => 'USD'
        ],

        'status' => [
            'new' => 'New',
            'processing' => 'Processing',
            'pending' => 'Pending',
            'confirmed' => 'Confirmed'
        ],

        'payment-method' => [
            'cash-on-delivery' => 'Cash On Delivery',
            'kbz pay' => 'KBZ Pay',
            'credit' => 'Credit'
        ]
    ];

?>