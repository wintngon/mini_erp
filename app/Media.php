<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Media extends Model
{
    use SoftDeletes;
    protected $table = 'media_library';

    protected $fillable = ['product_id', 'file_name', 'file_path', 'file_extension', 'file_type', 'file_size'];

    public function product(){
        return $this->belongsTo('App\Product');
    }
}
