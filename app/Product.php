<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = ['supplier_id', 'name', 'SKU', 'description', 'stock_status', 'price', 'currency', 'quantity', 'discount_price', 'discount_from', 'discount_to'];

    public function supplier(){
        return $this->belongsTo('App\Supplier');
    }

    public function categories(){
        return $this->belongsToMany('App\Category')->withTimestamps();
    }

    public function attributes(){
        return $this->belongsToMany('App\Attribute')->withPivot('values')->withTimestamps();
    }

    public function media(){
        return $this->hasMany('App\Media');
    }

    public function orders(){
        return $this->belongsToMany('App\Order')->withTimestamps();
    }
}
