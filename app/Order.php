<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $fillable = ['order_no', 'channel', 'status', 'customer_id', 'payment_method', 'total_amount'];

    public function products(){
        return $this->belongsToMany('App\Product')->withPivot('product_id', 'price', 'quantity', 'discount_price', 'total_amount')->withTimestamps();
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }
}
