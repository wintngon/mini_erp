@extends('layouts.admin')

@section('content')

{{ Breadcrumbs::render('values', $attribute) }}

	<h1>Attribute Value</h1>

    @include('includes.message')

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

    <ul class="nav nav-tabs">
		<li role="presenstation" class="active"><a href="#view"><strong>Attribute Value Lists - View</strong></a></li>
	</ul>

    <div class="container " style="padding: 10px;">
        <div class="row clearfix">
            <div class="col-md-12">
                <a href="{{ route('admin.attributes.index') }}" class="btn btn-warning" style="float:right; margin-bottom:20px;">Back to Attribute</a>   
            </div>
        </div>
        
        <div class="row clearfix">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Attribute Value Lists
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped text-center">
                            <thead">
                                <th class="text-center">ID</th>
                                <th class="text-center">Values</th>
                                <th class="text-center">Actions</th>
                            </thead>

                            <tbody>
                            <?php $i=0; ?>
                                @foreach($values as $value)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>
                                        <a href="{{ route('admin.attribute-values.edit', ['id'=>$value->id,'attr_id'=>$value->attribute->id]) }}" class="btn btn-primary btn-xs"><i class="material-icons">edit</i></a>

                                        @if($value->trashed())
                                            <form action="{{ route('admin.attribute-values.restore', $value->id) }}" method="get">
                                            @csrf
                                                <button class="btn btn-warning btn-xs"><i class="material-icons">restore</i></button>
                                            </form>
                                        @else
                                            <form action="{{ route('admin.attribute-values.destroy', ['id'=>$value->id,'attr_id'=>$value->attribute->id]) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                                <button class="btn btn-danger btn-xs"><i class="material-icons">delete</i></button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $values->links() }}
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        New Attribute Value
                    </div>

                    <div class="panel-body">
                        <form action="{{ route('admin.attribute-values.store') }}" method="POST" class="form-horizontal">
                        @csrf

                            <div class="row clearfix">
                                <div class="form-group col-md-12">
                                    <label for="attribute_id" class="control-label">Attribute Type</label>
                                    <select id="attribute_id" name="attribute_id" class="form-control">
                                        <option value="{{$attribute->id}}" selected>{{ $attribute->name }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="form-group col-md-12">
                                    <label for="name" class="control-label">Attribute Value</label><br>
                                    <input type="text"  class="form-control" name="name" id="name"/>
                                </div>
                            </div>

                            <div class="row clearfix" style="float:right;">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-plus"></i> Add
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection