@extends('layouts.admin')

@section('content')

	<h1>Supplier</h1>

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif

    <!-- Start .nav nav-tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presenstation"><a><strong>Edit Supplier</strong></a></li>
	</ul>
	<!-- End .nav nav-tabs -->
    <div class="container " style="padding: 10px;">
        <div class="col-md-12">
            <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit Supplier
                    </div>

                    <div class="panel-body">
                        <!-- Edit Supplier -->
                        <form action="{{ route('admin.suppliers.update', $supplier->id) }}" method="POST" class="form-horizontal">
                        @csrf
                        @method('PATCH')

                            <div class="row clearfix">
                                <div class="form-group col-md-12">
                                    <label for="name" class="control-label">Name</label>
                                    <input type="text" name="name" id="name" class="form-control" value="{{ $supplier->name }}">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="address1" class="control-label">Address 1</label>
                                    <input type="text" name="address1" id="address1" class="form-control" value="{{ $supplier->address1 }}">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="address2" class="control-label">Address 2</label>
                                    <input type="text" name="address2" id="address2" class="form-control" value="{{ $supplier->address2 }}">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="phone" class="control-label">Phone</label>
                                    <input type="text" name="phone" id="phone" class="form-control" value="{{ $supplier->phone }}">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="contact" class="control-label">Contact Person</label>
                                    <input type="text" name="contact" id="contact" class="form-control" value="{{ $supplier->contact_person }}">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="email" class="control-label">Mail</label>
                                    <input type="email" name="email" id="email" class="form-control" value="{{ $supplier->mail }}">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="description" class="control-label">Description</label>
                                    <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ $supplier->description }}</textarea>
                                </div>
                            </div>

                            <div class="row clearfix" style="float:right;">
                                <button type="submit" class="btn btn-success">
                                    Update
                                </button>
                            </div>
                        </form>
                    </div>
            </div>
        </div>
    </div>

@endsection
